﻿using UnityEngine;
using System.Collections;

public class ReallocScript : MonoBehaviour {
    GameObject GameManager;
    bool isReSelect;
    float goUpSpeed;
	// Use this for initialization
	void Start () {
        GameManager = GameObject.Find("GameManager");
        isReSelect = false;
        goUpSpeed = 10;
    }
    public void reSelectButton() {
        GameManager.GetComponent<UIManager>().selectedButton = -1;
    }
    public void noReSelectButton() {
        if (!isReSelect) {
            isReSelect = true;
            StartCoroutine(upMenu());
        }
    }

    void Update() {
        if (transform.FindChild("Slimenum").gameObject.transform.localScale.y >= 1)
            isReSelect = true;
    }

    public void intButton1()
    {
        GameManager.GetComponent<UIManager>().selectedButton = 1;
    }
    public void intButton2()
    {
        GameManager.GetComponent<UIManager>().selectedButton = 2;
    }
    public void intButton3()
    {
        GameManager.GetComponent<UIManager>().selectedButton = 3;
    }
    public void intButton4()
    {
        GameManager.GetComponent<UIManager>().selectedButton = 4;
    }
    public void intButton5()
    {
        GameManager.GetComponent<UIManager>().selectedButton = 5;
    }

    IEnumerator upMenu() {
        GameObject upper = transform.FindChild("Slimealloc").gameObject;
        GameObject lower = transform.FindChild("Slimenum").gameObject;
        for (float goUp = 0; goUp < 275; goUp += goUpSpeed) {
            upper.GetComponent<RectTransform>().anchoredPosition += new Vector2(0, goUpSpeed);
            lower.GetComponent<RectTransform>().localScale += new Vector3(0, goUpSpeed / 275);
            yield return new WaitForFixedUpdate();
        }
        upper.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -1245);
        lower.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
    }
}
