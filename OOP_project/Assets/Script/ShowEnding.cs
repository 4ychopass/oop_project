﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowEnding : MonoBehaviour
{
    // 플레이어 매니저에서 playerprefs에 저장하는거 빼기
    private string[] EndingList;
    private string endingName;
    private string endingContent;

    private int FriendNum;
    private float grade;
    private int semester;

    // Use this for initialization
    void Start()
    {
        if (Application.loadedLevelName == "endingScene")
        {
            showEnding();
        }
        else if (Application.loadedLevelName == "EndingList")
        {

            EndingList = new string[5];
            string temp;
            for (int i = 0; i < 5; i++)
            {
                temp = "ending";
                temp += (i + 1).ToString();
                EndingList[i] = PlayerPrefs.GetString(temp);

                Debug.Log(EndingList[i].CompareTo(""));
                if (EndingList[i].CompareTo("") == 0)
                {
                    
                    GameObject.Find(temp).GetComponent<Button>().interactable = false;
                    Debug.Log("false");
                }
                    
            }

        }
    }

    public void showEnding()
    {
        // 플레이어 매니저에 있는 엔딩 관련 text들을 찾아와서 저장
        endingName = GameObject.Find("PlayerManager").GetComponent<PlayerManager>().endingName;
        endingContent = GameObject.Find("PlayerManager").GetComponent<PlayerManager>().endingContent;

        PlayerPrefs.SetString(endingName, endingContent);
        // key와 value를 같게 해서 playerprefs에 저장
        // 같은 오브젝트 내에 있는 text에 저장
        GameObject.Find("endingName").GetComponent<Text>().text = endingName;
        GameObject.Find("endingContent").GetComponent<Text>().text = endingContent;


        FriendNum = (int)GameObject.Find("PlayerManager").GetComponent<PlayerManager>().socialStat;
        grade = GameObject.Find("PlayerManager").GetComponent<PlayerManager>().inteligenceStat;
        semester = GameObject.Find("PlayerManager").GetComponent<PlayerManager>().semester;

        string temp = "사귄 친구: ";
        temp += FriendNum.ToString();
        temp += "명";
        GameObject.Find("FriendNum").GetComponent<Text>().text = temp;

        string key = endingName;
        key += "FriendNum";
        PlayerPrefs.SetString(key, temp);

        temp = "학점: ";
        temp += grade.ToString();
        temp += ",  졸업학기: ";
        temp += semester.ToString();
        temp += "학기";
        GameObject.Find("Stats").GetComponent<Text>().text = temp;

        key = endingName;
        key += "Stats";
        PlayerPrefs.SetString(key, temp);
        key = endingName;
        key += "semester";
        PlayerPrefs.SetFloat(key, semester);
        if (semester>=8)
        {
            DestroyObject(GameObject.Find("Jogi"));
        }

        if (GameObject.Find("PlayerManager").GetComponent<PlayerManager>().playerProfile != null)
        {
            Sprite playerPic = Sprite.Create(GameObject.Find("PlayerManager").GetComponent<PlayerManager>().playerProfile, new Rect(0, 0, 1040, 840), new Vector2(0, 1));
            GameObject.Find("playerpic").GetComponent<Image>().sprite = playerPic;
        }

    }
    public void goMenu()
    {
        
        // 게임이 끝나면 새 게임을 하기 위해 세이브 후 start화면으로 넘어가도록 함
        if (Application.loadedLevelName == "endingScene")
        {
            GameObject.Find("PlayerManager").GetComponent<PlayerManager>().Save();
        }
        Application.LoadLevel("Start");
    }
    public void back()
    {
        GameObject.Find("EndingScene").GetComponent<RectTransform>().anchoredPosition = new Vector3(1080, 0, 0);
    }

    public void showEndingList(int index)
    {
        GameObject.Find("EndingScene").GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0,-500);
        // endinglist[index-1] 에 저장된 string을 key로 사용하면 다른 값들도 모두 표현 가능
        string temp = EndingList[index - 1];

        string key;
        GameObject.Find("endingName").GetComponent<Text>().text = temp;
        key = temp;
        GameObject.Find("endingContent").GetComponent<Text>().text = PlayerPrefs.GetString(key) ;
        key += "FriendNum";
        GameObject.Find("FriendNum").GetComponent<Text>().text = PlayerPrefs.GetString(key);
        key = temp;
        key += "Stats";
        GameObject.Find("Stats").GetComponent<Text>().text = PlayerPrefs.GetString(key);
        key = temp;
        key += "semester";
   
        if (PlayerPrefs.GetInt(key)>=8)
        {
            DestroyObject(GameObject.Find("Jogi"));
        }
    }

}
