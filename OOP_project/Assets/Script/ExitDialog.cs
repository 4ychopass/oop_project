﻿using UnityEngine;
using System.Collections;

public class ExitDialog : MonoBehaviour {

    public void Cancel() {
        Destroy(gameObject);
    }

    public void Confirm() {
        
        GameObject.Find("PlayerManager").GetComponent<PlayerManager>().Save();
        Debug.Log("Quit!");
        Application.LoadLevel("Start");
        Application.Quit();
    }
}
