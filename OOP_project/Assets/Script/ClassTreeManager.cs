﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class ClassTreeManager : MonoBehaviour {

    public int NUM;//전체 과목수
    public ClassNode rootnode;
    
    public ArrayList Class;//과목의 목록을 저장할 List*

    public GameObject node;//과목의 노드
    public GameObject Signed;//수강한 과목을 가지고 있는 게임오브젝트
    TextAsset nodeinfo;//과목정보의 파일
    TextAsset edgeinfo;//과목들의 연결관계들을 저장한 파일

    // Use this for initialization
    void Start () {
        NUM = 0;
        Class = new ArrayList();//리스트를 초기화 해준다.
        Signed = GameObject.Find("SignedClassList");//이미 수강한 과목의 정보를 불러온다.
        if (Signed.GetComponent<SignedClassList>().department == 1)//학과를 컴퓨터공학과를 선택한 경우
        {
            nodeinfo = Resources.Load("nodeinfo") as TextAsset;
            edgeinfo = Resources.Load("edgeinfo") as TextAsset;
            //컴퓨터공학과의 학과 트리가 저장된 파일을 불러온다.
        }
        else if (Signed.GetComponent<SignedClassList>().department == 2)//학과를 전자전기공학과를 선택한 경우
        {
            nodeinfo = Resources.Load("nodeinfo2") as TextAsset;
            edgeinfo = Resources.Load("edgeinfo2") as TextAsset;
            //전자전기공학과의 학과 트리가 저장된 파일을 불러온다.
        }
        /*else
        {
            nodeinfo = Resources.Load("nodeinfo2") as TextAsset;
            edgeinfo = Resources.Load("edgeinfo2") as TextAsset;
            //에러를 방지하기 위해 디폴트로 아무거나 불러온다. 이경우에는 전자과의 파일을 불러왔다.
        }*/
        GameObject oldclass = GameObject.Find("SignedClassList");//이전에 수강한 과목의 정보를 가진다.
        ArrayList Old = new ArrayList();//리스트 초기화
        oldclass.GetComponent<SignedClassList>().list = new ArrayList();//리스트 초기화
        oldclass.GetComponent<SignedClassList>().data_list = new ArrayList();//리스트 초기화
        if (oldclass.GetComponent<SignedClassList>().oldList==null)//수강한 과목이 없을때
        {oldclass.GetComponent<SignedClassList>().Start();}//초기화 시켜준다.
        Old = oldclass.GetComponent<SignedClassList>().oldList;//수강한 과목의 정보를 복사해 온다
        StringReader sr = new StringReader(nodeinfo.text);//파일을 읽어오기 위한 string Reader이다. 과목정보를 읽는다.
        string source = sr.ReadLine();//한 줄씩 읽는다.
        string[] values;//단어별로 저장한다.
        while (source !=null)//파일의 끝까지 읽는다.
        {
            ClassNode tmp= new ClassNode();//노드 초기화
            tmp.Start();//노드 초기화
            values= source.Split(' ');//띄어쓰기를 기준으로 구분한다.
            if (values.Length == 0)//아무것도 없다면 파일을 닫는다.
            {
                sr.Close();
                return;
            }
            tmp.Classname = values[0];
            tmp.level = stringtonum(values[1]);
            tmp.Maxpoint = stringtonum(values[2]);
            tmp.MaxSlime = stringtonum(values[3]);
            tmp.RunTime = stringtofloat(values[4]);
            tmp.x = stringtofloat(values[5]);
            tmp.y = stringtofloat(values[6]);
            //파일의 형식에 맞추어 읽어온다. 파일은 항상 올바른 형식이라고 가정한다.
            tmp.num = NUM;//과목에 번호를 붙인다.(인덱싱한다.)
            tmp.isselect = false;//과목으 수강신청 되었는가?(생성될때는 수강신청되어있지 않다.)
            
            tmp.parent = new ArrayList();//부모의 리스트를 초기화한다.
            tmp.child = new ArrayList();//자식의 리스트도 초기화한다.
            int flag = 0;//수강한 과목인지 알아보기 위한 변수
            for(int i=0;i<Old.Count;i++)
            {
                if (((ClassNode)Old[i]).Classname == tmp.Classname)//과목의 이름으로 확인한다. 이미 수강한 과목이라면 flag가 1이 된다.
                {
                    flag = 1;
                }
            }
            if (flag == 1) {
                tmp.isdone = true;//수강정보를 완료로 설정한다.
                GameObject.Find("Scrollbar").GetComponent<Scrollbar>().value = (float)(tmp.x / 15f);
                //이 노드에 맞추어 스크롤을 이동시킨다.

                    }//이미 수강한 과목이라면 
            else tmp.isdone = false;//수강하지 않았다면 수강정보를 수강하지 않음으로 설정한다.
            
            source = sr.ReadLine();//다음줄을 읽어온다.

            MakeNode(tmp);//이제까지 읽은 정보를 List에 저장한다.

        }
        sr = new StringReader(edgeinfo.text);//과목들의 연결관계를 읽는다.
        source = sr.ReadLine();//한줄씩
        while (source != null)//파일의 끝까지
        {
            values = source.Split(' ');//띄어쓰기를 기준으로 구분한다.
            if (values.Length == 0)
            {
                sr.Close();
                return;
            }//비어있다면 종료한다.
            int p = stringtonum(values[0]);//부모 과목의 인덱스이다.
            int c = stringtonum(values[1]);//자식 과목의 인덱스이다.
            MakeEdge((ClassNode)Class[p], (ClassNode)Class[c]);//부모와 자식을 이어주는 연결관계를 생성한다.
            source = sr.ReadLine();//다음줄을 읽는다.
        }

        GameObject.Find("SignedClassList").GetComponent<SignedClassList>().allList = Class;

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public ClassNode ReturnNode(ClassNode NODE) { return NODE; }

    void MakeEdge(ClassNode head, ClassNode tail)//두 과목의 연결관계를 생성한다.
    {
        head.childnum++;//부모 과목은 자식의 수를 늘린다.
        tail.parentnum++;//자식 과목은 부모의 수를 늘린다.
        head.child.Add(tail);//부모 과목에 자식과목을 추가한다.
        tail.parent.Add(head);//자식과목에 부모과목을 추가한다.
        
    }

    void MakeNode(ClassNode newclass)//과목을 리스트에 저장한다.
    {
        NUM++;//과목의 수를 늘린다.
        Class.Add(newclass);//과목을 추가한다.
    }

    int stringtonum(string a)//문자열을 정수형 변수로 변환해 준다. 문자열은 잘못 들어오지 않는다고 가정한다.
    {
        int ans = 0;//일단 0부터 시작
        for (int i=0 ; i< a.Length ; i++)//문자열의 끝까지
        {
            ans *= 10;
            ans += a[i]-48;//숫자를 만들어간다.
        }
        return ans;//변환값을 반환한다.
    }
    float stringtofloat(string a)//문자열을 실수형 변수로 변환한다. 문자열은 잘못 들어오지 않는다과 가정한다.
    {
        float flag;//음수인지를 확인하기 위한것
        if (a[0] == 45) flag = -1;//음수일때
        else flag = 1;//음수가 아닐때
        float ans = 0;
        bool dot = false;//소숫점을 찾는다.
        float underten = 1;//소숫점아래 몇째 자리인지 알려준다.
        for (int i = 0; i < a.Length; i++)//문자열의 끝까지
        {
            if (dot == false)//소숫점이 나오지 않았다면
            {
                if (a[i] == 46) { dot = true; }//소숫점이 나오면 소숫점이 나왔다고 알린다.
                else if (a[i] != 45)//그렇지 않은경우 정수부분을 계속 만들어간다.
                {
                    ans *= 10;
                    ans += a[i] - 48;
                }
            }
            else if (dot == true)//소숫점이 나온 이후
            {
                if (a[i] != 45)
                {
                    underten /= 10;//계속 아래로
                    ans += (a[i] - 48) * underten;//뒤에 붙여준다.
                }//소숫점 아래를 붙여나간다.
            }
        }
        return flag * ans;//값을 반환한다. 음수일경우 -1이 곱해져 음수로 반환된다.
    }

    ClassNode copy(ClassNode a)//과목복사
    {
        return a;
    }

}
