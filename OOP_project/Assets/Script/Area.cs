﻿using UnityEngine;
using System.Collections;

public abstract class Area : MonoBehaviour {

    public Area[] connArea;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public abstract bool isDestination();

    public abstract Area nextArea(Area searchArea, DestArea destination);
}
