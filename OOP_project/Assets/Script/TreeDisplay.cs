﻿using UnityEngine;
using System.Collections;

public class TreeDisplay : MonoBehaviour {

    public GameObject Origin_Node;
    public GameObject data;
    public GameObject line;
    public GameObject Node_Background;//수강한 노드의 배경이 될 이미지
    public GameObject Node_Undo;//수강하지 않은 노드의 배경이 될 이미지
    public GameObject Node_Check;//수강신청한 과목에 체크 표시를 하기위한 체크 이미지
    public GameObject Node_Name;//과목의 이름을 텍스트로 표시하기 위한 오브젝트
    public GameObject Node_Level;//과목의 난이도를 표시하기 위한 오브젝트

    ArrayList NodeList;//과목들의 리스트이다.(이 경우에는 과목정보를 가지고 있는 게임오브젝트들)

    bool ismake=false;
    bool flag = false;
    float Pi = 3.141592f;

    int NUM=200;
    // Use this for initialization

    public void Start () {
        data = GameObject.Find("ClassTreeManager");//과목정보를 받아온다.
        NodeList = new ArrayList();
        for(int i=0;i<data.GetComponent<ClassTreeManager>().NUM;i++)
        {
            AddNode(i);//모든 과목정보를 옮겨온다.
        }
	
	}
	
	// Update is called once per frame
	void Update () {
        if (!ismake)//만들어지지 않았을 경우
        {
            data = GameObject.Find("ClassTreeManager");
            for (int i = 0; i < data.GetComponent<ClassTreeManager>().NUM; i++)
            {
                AddNode(i);
            } 
        }
        
        if(!flag)//한번만 생성하기 위한것이다. start로 실행할 경우 실행순서때문에 에러가 발생할 수 있어 따로 빼었다.
        {
            flag = true;
            StartCoroutine(drawLine());//코루틴 시작
        }
    }

    IEnumerator drawLine(){//과목을 연결해 주는 선을 그린다.
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < data.GetComponent<ClassTreeManager>().NUM; i++)
        {
            if (((GameObject)NodeList[i]).GetComponent<ClassNode>().child.Count > 0)
            {
                for (int j = 0; j < ((GameObject)NodeList[i]).GetComponent<ClassNode>().child.Count; j++)//자식들을 연결한다.
                {
                    GameObject newobject = Instantiate(line);//라인렌더러를 받아온다.
                    newobject.transform.parent = GameObject.Find("ClassManager").transform;//기준 오브젝트에 맞춘다.
                    newobject.transform.position = transform.position;//위치를 맞춘다.
                    newobject.GetComponent<LineRenderer>().SetVertexCount(NUM);
                    Vector3 startpos= ((GameObject)NodeList[i]).transform.position;//시작점은 부모과목의 좌표이다.
                    Vector3 endpos = ((GameObject)NodeList[((ClassNode)((GameObject)NodeList[i]).GetComponent<ClassNode>().child[j]).num]).transform.position;//끝점은 자식과목의 좌표이다.
                    startpos=startpos+new Vector3(0f, -0.4f, 0f);//노드의 크기에 맞추어 시작점을 옮긴다.
                    endpos = endpos + new Vector3(0f, -0.4f, 0f);//노드의 크기에 맞추어 끝점을 옮긴다.
                    float stringlen = (float)((GameObject)NodeList[i]).GetComponent<ClassNode>().Classname.Length;
                    startpos = startpos + new Vector3(2.5f, 0f, 0f);//노드의 크기에 맞추어 시작점을 옮긴다.
                    Vector3 distance = endpos - startpos;//둘 사이의 거리는 끝점에서 시작점을 뺀 것이다.
                    Vector3 trans = distance;//정보를 옮겨둔다.

                    for(int num =0;num< NUM; num+=1)
                    {
                        trans.Set(distance[0] * 1/(float)(NUM-1)*(float)num, distance[1] * Mathf.Pow(Mathf.Sin(1f /(float)(NUM - 1) * (float)num*Pi/2f),4), distance[2]);//연결해줄 좌표를 설정한다.(선을 sin^4형태로 만들었다.)
                        newobject.GetComponent<LineRenderer>().SetPosition(num, startpos + trans);//선을 그린다.
                    }
                }

            }

        }
    }

    void AddNode(int i)//과목의 게임오브젝트를 추가한다.
    {
        GameObject newobject = Instantiate(Origin_Node);
        GameObject background = Instantiate(Node_Background);
        GameObject undo = Instantiate(Node_Undo);
        GameObject NodeName = Instantiate(Node_Name);
        GameObject NodeCheck = Instantiate(Node_Check);
        GameObject NodeLevel = Instantiate(Node_Level);
        //각각의 자식오브젝트를 생성한다.
        newobject.transform.parent = GameObject.Find("ClassManager").transform;//기준에 맞추어 생성한다.
        background.transform.parent = newobject.transform;
        undo.transform.parent = newobject.transform;
        NodeName.transform.parent = newobject.transform;
        NodeCheck.transform.parent = newobject.transform;
        NodeLevel.transform.parent = newobject.transform;
        //부모 자식의 관계를 형성한다.
        newobject.GetComponent<ClassNode>().Start();//한번 초기화 해준다.
        newobject.GetComponent<ClassNode>().setdata((ClassNode)data.GetComponent<ClassTreeManager>().Class[i]);//정보를 입력한다.
        print(newobject.GetComponent<ClassNode>().Classname);
        
        NodeList.Add(newobject);//리스트에 추가한다.
        ismake = true;//만들어 졌음을 알린다.
    }
}
