﻿using UnityEngine;
using System.Collections;
using live2d;

public class Slime : MonoBehaviour
{

    private Area currentArea;
    public Area CurrentArea {
        get { return currentArea; }
    }

    public float slimeSpeed;

    public DestArea Destination;
    public Area destination {
        get {
            return Destination;
        }
    }

    //private Area nextArea;

    private float WorkTime;
    private float distance;
    public float workTime
    {
        get
        {
            return WorkTime;
        }
    }

    private Live2DModelUnity slimeModel;
    private Live2DMotion[] motion;
    private TextAsset mocFile;
    private Texture2D texture;
    private TextAsset[] motionFile;
    private MotionQueueManager motionManager;
    private bool draw;
    private GameObject SummonHole;
    private bool goUpDone;
    private bool teleportationDone;
    private int slimeType;

    private enum motions {
        BLINK = 0,
        BREATH  = 1,
        DRUNKEN = 2,
        GOUP = 3,
        PHONE = 4,
        SLIMEBOUNCE = 5,
        STUDYING = 6
    };

    // Use this for initialization
    void Start()
    {
        teleportationDone = true;
        distance = 0.20f;
        slimeType = Random.Range(1, 6);
        SummonHole = gameObject.transform.parent.FindChild("Hole").gameObject;
        mocFile = Resources.Load("Live2D/Model/Slime_"+slimeType+"/slime_"+slimeType+".moc") as TextAsset;
        texture = Resources.Load("Live2D/Model/Slime_" + slimeType + "/slime_" + slimeType + ".2048/texture_00") as Texture2D;
        motionFile = new TextAsset[7];
        motion = new Live2DMotion[7];
        motionFile[0] = Resources.Load("Live2D/Motions/Blink.mtn") as TextAsset;
        motionFile[1] = Resources.Load("Live2D/Motions/Breath.mtn") as TextAsset;
        motionFile[2] = Resources.Load("Live2D/Motions/Drunken.mtn") as TextAsset;
        motionFile[3] = Resources.Load("Live2D/Motions/GoUp.mtn") as TextAsset;
        motionFile[4] = Resources.Load("Live2D/Motions/Phone.mtn") as TextAsset;
        motionFile[5] = Resources.Load("Live2D/Motions/SlimeBounce.mtn") as TextAsset;
        motionFile[6] = Resources.Load("Live2D/Motions/Studying.mtn") as TextAsset;
        motionManager = new MotionQueueManager();
        Live2D.init();
        draw = true;
        slimeModel = Live2DModelUnity.loadModel(mocFile.bytes);
        for (int i = 0; i < 7; i++)
        {
            motion[i] = Live2DMotion.loadMotion(motionFile[i].bytes);
        }

        slimeModel.setTexture(0, texture);
        gameObject.transform.localPosition = new Vector3(-0.3f, -0.25f);
        currentArea = GameObject.Find("GameManager").GetComponent<SlimeManager>().destinationList[0];
        Destination = (DestArea)currentArea;
        transform.parent.position = currentArea.gameObject.transform.position;
        WorkTime = 0;
        goUpDone = false;
    }

    void OnRenderObject()
    {
        Matrix4x4 m1 = Matrix4x4.Ortho(-1024f, 1024f, 1024f, -1024f, -0.5f, 0.5f);
        Matrix4x4 m2 = transform.localToWorldMatrix;
        Matrix4x4 m3 = m2 * m1;

        slimeModel.setMatrix(m3);

        slimeModel.update();
        if (draw)
            slimeModel.draw();
    }

    // Update is called once per frame
    void Update()
    {
        if (teleportationDone && motionManager.isFinished()) {
            if (currentArea.Equals(GameObject.Find("GameManager").GetComponent<SlimeManager>().destinationList[0]))
            {
                switch (Random.Range(0, 2)) {
                    case 0:
                        motionManager.startMotion(motion[(int)motions.BREATH], true);
                        break;
                    case 1:
                        motionManager.startMotion(motion[(int)motions.PHONE], true);
                        break;
                }
            }
            else if (currentArea.Equals(GameObject.Find("GameManager").GetComponent<SlimeManager>().destinationList[1]))
            {
                motionManager.startMotion(motion[(int)motions.STUDYING], true);
            }
            else if (currentArea.Equals(GameObject.Find("GameManager").GetComponent<SlimeManager>().destinationList[2]))
            {
                motionManager.startMotion(motion[(int)motions.DRUNKEN], true);
            }
        }else if (!motionManager.isFinished()) {
            motionManager.updateParam(slimeModel);
        }
        if (GameObject.FindGameObjectWithTag("Profile"))
        {

            draw = false;
        }
        else {
            draw = true;
        }
    }

    public void setSlimeWork(Area destination, float workTime)
    {
        Destination = (DestArea)destination;
        StartCoroutine(startWork(workTime));
    }

    private IEnumerator startWork(float workTime)
    {
        StartCoroutine(Teleportation());
        yield return new WaitForSeconds(workTime);
        Destination = (DestArea)GameObject.Find("GameManager").GetComponent<SlimeManager>().destinationList[0];
        StartCoroutine(Teleportation());
    }

    private IEnumerator Teleportation() {
        motionManager.stopAllMotions();
        teleportationDone = false;
        currentArea = Destination;
        StartCoroutine(goUp());
        while(!goUpDone)
            yield return new WaitForFixedUpdate();
        goUpDone = false;
        gameObject.transform.parent.position = Destination.transform.position + (Vector3)Random.insideUnitCircle;
        StartCoroutine(goDown());
        
    }

    private IEnumerator goUp()
    {
        SummonHole.GetComponent<SpriteRenderer>().enabled = true;
        for (float i = 0; i < 0.075f; i += 0.009f)
        {
            yield return new WaitForSeconds(0.0075f);
            SummonHole.transform.localScale += new Vector3(0.009f, 0);
        }
        SummonHole.transform.localScale = new Vector3(0.075f, 0.075f);
        motionManager.startMotion(motion[(int)motions.GOUP], false);
        for (float i = 0; i < (1f / 3f); i += 0.03f)
        {
            //motionManager.updateParam(slimeModel);
            yield return new WaitForSeconds(0.03f);
        }
        float color = 1f;
        while (!motionManager.isFinished())
        {
            if (gameObject.transform.localPosition.y > distance)
                break;
            color -= 0.05f;
            gameObject.transform.position += new Vector3(0, 0.05f);
            //motionManager.updateParam(slimeModel);
            slimeModel.setTextureColor(0, color, color, color);
            yield return new WaitForFixedUpdate();
        }
        draw = false;

        for (float i = 0.075f; i > 0; i -= 0.009f)
        {
            yield return new WaitForSeconds(0.015f);
            SummonHole.transform.localScale -= new Vector3(0.009f, 0);
        }
        SummonHole.transform.localScale = new Vector3(0, 0.075f);
        SummonHole.GetComponent<SpriteRenderer>().enabled = false;
        goUpDone = true;
    }

    private IEnumerator goDown()
    {
        SummonHole.GetComponent<SpriteRenderer>().enabled = true;
        for (float i = 0; i < 0.075f; i += 0.009f)
        {
            yield return new WaitForSeconds(0.0075f);
            SummonHole.transform.localScale += new Vector3(0.009f, 0);
        }
        SummonHole.transform.localScale = new Vector3(0.075f, 0.075f);
        //차원의 문 열림

        slimeModel.setParamFloat("BOUNCE", 1);
        slimeModel.update();
        float color = 0f;
        draw = true;
        gameObject.transform.localPosition = new Vector3(-0.3f,distance);
        motionManager.startMotion(motion[(int)motions.SLIMEBOUNCE], false);
        while (gameObject.transform.localPosition.y > -0.25f)
        {
            if (color < 1)
            {
                color += 0.05f;
            }
            else
            {
                color = 1;
            }
            gameObject.transform.localPosition -= new Vector3(0, 0.05f);
            slimeModel.setTextureColor(0, color, color, color);
            //motionManager.updateParam(slimeModel);
            yield return new WaitForFixedUpdate();
        }
        gameObject.transform.localPosition = new Vector3(-0.3f, -0.25f);
        slimeModel.setTextureColor(0, 1, 1, 1);


        while (!motionManager.isFinished())
        {
            //motionManager.updateParam(slimeModel);
            yield return new WaitForFixedUpdate();
        }

        for (float i = 0.075f; i > 0; i -= 0.009f)
        {
            yield return new WaitForSeconds(0.015f);
            SummonHole.transform.localScale -= new Vector3(0.009f, 0);
        }
        SummonHole.transform.localScale = new Vector3(0, 0.075f);
        SummonHole.GetComponent<SpriteRenderer>().enabled = false;
        teleportationDone = true;
    }
}
