﻿using UnityEngine;
using System.Collections;

public class SocialEvent : MonoBehaviour {

    int snum;//slimenum
    int maxslime;//MaxSlime
    int minslime;//MixSlime
    string ename;//eventname
    float wtime;//workTime
    float dtime;//delay
    float fulltime;//fullTime
    float spoint;//point
    SocialManager.groupname gname;//groupname


    //public property
    public int slimenum { get { return snum; } }//가지고 있는 슬라임 수
    public int MaxSlime { get { return maxslime; } }//최대 가지고 있을 수 있는 슬라임 수
    public int MinSlime { get { return minslime; } }//최소 가지고 있을 슬라임 수
    public string eventname { get { return ename; } }//발생된 이벤트의 이름
    public float workTime { get { return wtime; } set { wtime = value; } }//작업 종료까지 남은 시간
    public float delay { get { return dtime; } set { dtime = value; } }//이벤트를 실행하기까지 유효 시간
    public float fullTime { get { return fulltime; } }//총 작업 시간
    public float point { get { return spoint; } }//이벤트의 점수
    public SocialManager.groupname groupname { get { return gname; } }//이벤트가 발생된 주체 그룹

    //public variable
    public GameObject Location;//발생 지역 (UI)
    public bool canceled;
 
    bool isH;//실행 체크
    bool eventmade;//생성 체크
    bool working;//작업중 표시
    bool delaying;//선택대기중 표시

	void Start () {
        snum = -1;
        ename = null;
        wtime = 0f;
        dtime = 0f;
        spoint = 0.1f;

        isH = false;
        eventmade = false;
        working = false;
        delaying = false;

        canceled = false;
	}

    void Update()
    {
        //인맥 이벤트 활동 시작(흐름상 인맥이벤트 생성 이후 발생)
        if (isH)
        {
            destroyUI();
            Location = GameObject.Find("GameManager").GetComponent<UIManager>().createSocialLocation(445,-634);
            isH = false;
            eventmade = false;
            working = true;
            delaying = false;
        }
        //인맥 이벤트 생성을 알림
        else if (eventmade)
        {
            Location = GameObject.Find("GameManager").GetComponent<UIManager>().createSocialLocation(0,0);
            delaying = true;
            eventmade = false;
        }
        //시간이 지나면 Social Event가 자동적으로 제거됨
        else if (canceled || (working && wtime == 0f)||(dtime==0f && delaying))
        {
            destroyUI();
            Destroy(gameObject);
        }
    }

    //이벤트 생성
    public void makeEvent(SocialManager.groupname group, string eventname,int mins, int maxs, float eventTime) {
        System.Random r = new System.Random();
        snum = r.Next(mins, maxs);
        maxslime = maxs;
        minslime = mins;
        ename = eventname;
        wtime = eventTime;
        dtime = 10f;
        fulltime = eventTime;
        gname = group;

        eventmade = true;
    }

    //이벤트 실행
    public bool setEvent() {
        dtime = 0f;
        spoint = 0.4f * snum;//balance

        //슬라임 일시키기
        bool ret=true;
        ret = GameObject.Find("GameManager").GetComponent<SlimeManager>().setSlimeWork(SlimeManager.DESTLIST.DESTINATION_HJ_MRK,slimenum,wtime);
        if(!ret) GameObject.Find("GameManager").GetComponent<UIManager>().showMessage("슬라임의 개수가 부족합니다.");
        else isH = true;
        return ret;
    }

    //이벤트 바로 실행(생성 + 실행)
    public bool setEvent(SocialManager.groupname group, string eventname, int MaxSlimeNum, float eventTime)
    {
        System.Random r = new System.Random();
        snum = r.Next(1, MaxSlimeNum);
        maxslime = MaxSlimeNum;
        ename = eventname;
        wtime = eventTime;
        dtime = 0f;
        fulltime = eventTime;
        spoint = 0.4f * snum;//balance
        gname = group;

        //슬라임 일시키기
        bool ret=true;
        ret = GameObject.Find("GameManager").GetComponent<SlimeManager>().setSlimeWork(SlimeManager.DESTLIST.DESTINATION_HJ_MRK,slimenum,wtime);
        if(!ret) GameObject.Find("GameManager").GetComponent<UIManager>().showMessage("슬라임의 개수가 부족합니다.");
        else
        {
            isH = true;
            eventmade = true;
        }
        return ret;
    }

    //UI 제거
    public void destroyUI()
    {
        if(Location!=null)
        Destroy(Location);
    }
}
