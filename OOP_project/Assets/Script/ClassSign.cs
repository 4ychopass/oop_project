﻿using UnityEngine;
using System.Collections;

public class ClassSign : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Check()
    {
        GameObject yes = GameObject.Find("SignYes");//YES버튼
        GameObject no = GameObject.Find("SignNo");//NO버튼
        GameObject text = GameObject.Find("SignText");//텍스트
        GameObject back = GameObject.Find("Image");//배경이미지
        GameObject pan = GameObject.Find("Panel");//패널(뒤쪽 기능가리기)

        back.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        yes.GetComponent<RectTransform>().localPosition = new Vector3(-170, -50, 1);
        no.GetComponent<RectTransform>().localPosition = new Vector3(170, -50, 1);
        text.GetComponent<RectTransform>().localPosition = new Vector3(0, 100, 1);
        pan.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        //각각의 게임오브젝트의 위치를 설정한다.

    }
    public void No()
    {
        GameObject yes = GameObject.Find("SignYes");
        GameObject no = GameObject.Find("SignNo");
        GameObject text = GameObject.Find("SignText");
        GameObject back = GameObject.Find("Image");
        GameObject pan = GameObject.Find("Panel");

        yes.GetComponent<RectTransform>().localPosition = new Vector3(10000, 10000, 1);
        no.GetComponent<RectTransform>().localPosition = new Vector3(10000, 10000, 1);
        back.GetComponent<RectTransform>().localPosition = new Vector3(10000, 10000, 1);
        text.GetComponent<RectTransform>().localPosition = new Vector3(10000, 10000, 1);
        pan.GetComponent<RectTransform>().localPosition = new Vector3(10000, 10000, 1);

        //멀리 보내어 화면에서 사라지도록 한다.
    }
    public void yes()//수강신청을 완료하고 게임화면으로 넘어간다.
    {
        GameObject save = GameObject.Find("SignedClassList");
        DontDestroyOnLoad(save);
        for (int j = 0; j < save.GetComponent<SignedClassList>().list.Count; j++)
        {
            save.GetComponent<SignedClassList>().oldList.Add(save.GetComponent<SignedClassList>().list[j]);
        }//수강신청한 과목을 수강한 과목리스트에 추가한다.
        copydata();
        Application.LoadLevel("InGame");//게임화면으로 넘어간다.
        Debug.Log(save.GetComponent<SignedClassList>().list.Count);
    }
    private void copydata()
    {
        GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_list = new ArrayList();
        GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_oldList = new ArrayList();
        for (int i = 0; i < GameObject.Find("SignedClassList").GetComponent<SignedClassList>().list.Count; i++)
        {
            DataClassNode tmp = new DataClassNode();
            tmp.Classname = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().list[i]).Classname;
            tmp.level = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().list[i]).level;
            tmp.isdone = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().list[i]).isdone;
            tmp.MaxSlime = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().list[i]).MaxSlime;
            tmp.num = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().list[i]).num;
            tmp.Maxpoint = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().list[i]).Maxpoint;
            tmp.RunTime = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().list[i]).RunTime;

            GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_list.Add(tmp);
        }
        for (int i = 0; i < GameObject.Find("SignedClassList").GetComponent<SignedClassList>().oldList.Count; i++)
        {
            DataClassNode tmp = new DataClassNode();
            tmp.Classname = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().oldList[i]).Classname;
            tmp.level = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().oldList[i]).level;
            tmp.isdone = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().oldList[i]).isdone;
            tmp.MaxSlime = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().oldList[i]).MaxSlime;
            tmp.num = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().oldList[i]).num;
            tmp.Maxpoint = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().oldList[i]).Maxpoint;
            tmp.RunTime = ((ClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().oldList[i]).RunTime;

            GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_oldList.Add(tmp);
        }

    }
}
