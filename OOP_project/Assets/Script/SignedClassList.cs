﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System;

[Serializable]
public class SignedClassList : MonoBehaviour {

    public ArrayList list;//신청한 과목의 리스트이다.
    public ArrayList oldList;//수강했던 과목의 리스트이다.
    public int department;//어떤 학과인지에 대한 정보이다.

    public ArrayList data_list;
    public ArrayList data_oldList;

    public ArrayList allList;


	// Use this for initialization
	public void Start () {
        list = new ArrayList();//처음 시작될 때 초기화한다.

        oldList = new ArrayList();//처음시작될때 초기화한다.

        data_list = new ArrayList();

        data_oldList = new ArrayList();

        allList = new ArrayList();
        DontDestroyOnLoad(gameObject);


    }
	
	// Update is called once per frame
	void Update () {
        if (department ==1)
        {
            if (allList.Count == 0)
            {
                for (int i = 0; i < 12; i++)
                {
                    allList.Add(new ClassNode());
                }
            }
        }
        else if (department == 2)
        {
            if (allList.Count == 0)
            {
                for (int i = 0; i < 14; i++)
                {
                    allList.Add(new ClassNode());
                }
            }
        }
        if (list.Count == 0)
        {
            for (int i = 0; i < data_list.Count; i++)
            {
                ClassNode tmp = new ClassNode();
                tmp.setdataclass((DataClassNode)data_list[i]);
                list.Add(tmp);
            }
        }
        if (oldList.Count == 0)
        {
            for (int i = 0; i < data_oldList.Count; i++)
            {
                ClassNode tmp = new ClassNode();
                tmp.setdataclass((DataClassNode)data_oldList[i]);
                oldList.Add(tmp);
            }
        }
    }

    public void EndScene()
    {
        GameObject save = GameObject.Find("SignedClassList");//저장을 위해 정보를 저장할 게임오브젝트를 찾는다.
        DontDestroyOnLoad(save);//이 게임오브젝트를 씬이 넘어가도 파괴되지 않도록 한다.
        for (int j = 0; j < save.GetComponent<SignedClassList>().list.Count; j++)
        {
            save.GetComponent<SignedClassList>().oldList.Add(save.GetComponent<SignedClassList>().list[j]);
        }
        copydata();
        //수강신청한 과목을 수강완료한 과목의 리스트에 추가한다.
        Application.LoadLevel("InGame");//씬을 바꾼다.
        Debug.Log(save.GetComponent<SignedClassList>().list.Count);

    }
    public void gonext()//처음 시작할때 학과를 선택하는 화면으로 넘어간다.
    {
        GameObject save = GameObject.Find("SignedClassList");
        DontDestroyOnLoad(save);
        Application.LoadLevel("GetDepartment");//학과 선택화면으로 넘어간다.
    }
    public void computer()//컴퓨터 공학과를 선택한 경우
    {
        GameObject save = GameObject.Find("SignedClassList");
        DontDestroyOnLoad(save);
        save.GetComponent<SignedClassList>().department = 1;//학과를 컴퓨터 공학과로 설정한다.
        Application.LoadLevel("ClassTree");//수강신청 화면으로 넘어간다.
    }
    public void electric()//전자과를 선택한 경우
    {
        GameObject save = GameObject.Find("SignedClassList");
        DontDestroyOnLoad(save);
        save.GetComponent<SignedClassList>().department = 2;//학과를 전자과로 설정한다.
        Application.LoadLevel("ClassTree");//수강신청 화면으로 넘어간다.
    }
    private void copydata()
    {
        data_list = new ArrayList();
        data_oldList = new ArrayList();
        for(int i=0; i<list.Count;i++)
        {
            DataClassNode tmp = new DataClassNode();
            tmp.Classname = ((ClassNode)list[i]).Classname;
            tmp.level = ((ClassNode)list[i]).level;
            tmp.isdone = ((ClassNode)list[i]).isdone;
            tmp.MaxSlime = ((ClassNode)list[i]).MaxSlime;
            tmp.num = ((ClassNode)list[i]).num;
            tmp.Maxpoint = ((ClassNode)list[i]).Maxpoint;
            tmp.RunTime = ((ClassNode)list[i]).RunTime;

            data_list.Add(tmp);
        }
        for (int i = 0; i < oldList.Count; i++)
        {
            DataClassNode tmp = new DataClassNode();
            tmp.Classname = ((ClassNode)list[i]).Classname;
            tmp.level = ((ClassNode)list[i]).level;
            tmp.isdone = ((ClassNode)list[i]).isdone;
            tmp.MaxSlime = ((ClassNode)list[i]).MaxSlime;
            tmp.num = ((ClassNode)list[i]).num;
            tmp.Maxpoint = ((ClassNode)list[i]).Maxpoint;
            tmp.RunTime = ((ClassNode)list[i]).RunTime;

            data_oldList.Add(tmp);
        }

    }
}
