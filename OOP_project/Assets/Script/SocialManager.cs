﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SocialManager : MonoBehaviour {
    
    //public variable
    public enum groupname//그룹 이름
    {CSE = 101, EEE = 102,
        one = 201, two = 202, three = 203, four = 204, five = 205, six = 206, seven = 207, eight = 208,
        nine = 209, ten = 210, eleven = 211, twelve = 212, thirteen = 213, fourteen = 214, fifteen = 215,
        performance = 301, scholarship, sports, organization};
    public GameObject SocialEventObject;//event 생성용

    private ArrayList group;// = new ArrayList();
    private ArrayList eventlist = new ArrayList();
    private ArrayList preplist = new ArrayList();
    private int WorkingSlime;

    //public property
    public ArrayList InGroup { get { return this.group; } set { group = value; } }//가입된 그룹
    public ArrayList EventList { get { return this.eventlist; } set { eventlist = value; } }//실행 중인 이벤트
    public ArrayList PrepList { get { return this.preplist; } set { preplist = value; } }//선택을 기다리는 이벤트
    public int workingSlime { get { return WorkingSlime; } }//현재 작업 중인 총 슬라임 수

    float accumsocial;
    GameObject clone;
    GameObject eventpopup;
    bool[] ckfevent;
    bool[] randevent;
    bool semester;
    bool eventon;
    bool[] join;

    static double z0, z1;
    static bool g;


	void Start () {
        group = GameObject.Find("PlayerManager").GetComponent<PlayerManager>().groupList;//new ArrayList();
        eventlist = new ArrayList();
        preplist = new ArrayList();
        accumsocial = 0;
        clone = Instantiate(SocialEventObject);
        eventpopup = Instantiate(SocialEventObject);
        ckfevent = new bool[38];
        for (int i = 0; i < 38; i++)
        {
            ckfevent[i] = false;
        }
        randevent = new bool[7];
        for (int i = 0; i < 7; i++)
        {
            randevent[i] = false;
        }
        semester = false;
        WorkingSlime = 0;
        eventon = false;
        join = new bool[8];
        for (int i = 0; i < 8; i++) join[i] = true;
        for (int i = 0; i < group.Count; i++)
        {
            string str = (string)group[i];
        }
	}



    void Update()
    {
        startjoining();
       
        if (semester == false && GetComponent<TimeManager>().semesterStart == true)
        {
            semester = GetComponent<TimeManager>().semesterStart;
            StartCoroutine(CheckEvent());
        }
        else
            semester = GetComponent<TimeManager>().semesterStart;
    }

    IEnumerator CheckEvent()
    {
        while (true)
        {
            if (GetComponent<TimeManager>().semesterStart)
            {
                checkdelay();
                checkwork();
                checkappointedevent();
                checkrandomevent();
                checkclick();
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    void startjoining()
    {
        if (GameObject.Find("PlayerManager").GetComponent<PlayerManager>().semester == 1&&join[0])
        {
            if (join[1])
            {
                GetComponent<TimeManager>().pause();
                if(isIn(groupname.performance.ToString())) { join[1] = false; join[2] = false; }
                else { eventpopup = GetComponent<UIManager>().showEventPop("공연동아리가입", "공연동아리에서 동아리원을 모집합니다", 0, 0f);
                    join[1] = false;
                }
            }
            else if (eventpopup!=null&&join[2])
            {
                if ((eventpopup).GetComponent<EventPop>().status != EventPop.STATUS.NOTSELECT)
                {
                    if ((eventpopup).GetComponent<EventPop>().status == EventPop.STATUS.CONFIRM)
                        getIn(groupname.performance);
                    Destroy(eventpopup);
                    join[2] = false;
                }
            }
            else if(join[3]){
                if (isIn(groupname.scholarship.ToString())) { join[3] = false; join[4] = false; }
                else
                {
                    eventpopup = GetComponent<UIManager>().showEventPop("학술동아리가입", "학술동아리에서 동아리원을 모집합니다", 0, 0f);
                    join[3] = false;
                }
            }
            else if (eventpopup!=null&&join[4])
            {
                if ((eventpopup).GetComponent<EventPop>().status != EventPop.STATUS.NOTSELECT)
                {
                    if ((eventpopup).GetComponent<EventPop>().status == EventPop.STATUS.CONFIRM)
                        getIn(groupname.scholarship);
                    Destroy(eventpopup);
                    join[4] = false;
                }
            }
            else if(join[5])
            {
                if (isIn(groupname.sports.ToString())) { join[5] = false; join[6] = false; }
                else
                {
                    eventpopup = GetComponent<UIManager>().showEventPop("운동동아리가입", "운동동아리에서 동아리원을 모집합니다", 0, 0f);
                    join[5] = false;
                }
            }
            else if (eventpopup!=null&&join[6])
            {
                if ((eventpopup).GetComponent<EventPop>().status != EventPop.STATUS.NOTSELECT)
                {
                    if ((eventpopup).GetComponent<EventPop>().status == EventPop.STATUS.CONFIRM)
                        getIn(groupname.sports);
                    Destroy(eventpopup);
                    join[6] = false;
                }
            }
            else if(join[7])
            {
                if (isIn(groupname.performance.ToString())) {
                    join[7] = false;
                    join[0] = false;
                    groupUI();
                    GetComponent<TimeManager>().go();
                }
                else
                {
                    eventpopup = GetComponent<UIManager>().showEventPop("자치단체가입", "자치단체에서 동아리원을 모집합니다", 0, 0f);
                    join[7] = false;
                }
            }
            else
            {
                if ((eventpopup).GetComponent<EventPop>().status != EventPop.STATUS.NOTSELECT)
                {
                    if ((eventpopup).GetComponent<EventPop>().status == EventPop.STATUS.CONFIRM)
                        getIn(groupname.organization);
                    Destroy(eventpopup);
                    join[0] = false;
                    groupUI();
                    GetComponent<TimeManager>().go();
                }
            }
        }
        else if (join[0])
        {
            groupUI();
            join[0] = false;
        }
    }

    void checkdelay()
    {
        for (int i = 0; preplist != null && i < preplist.Count; i++)
        {
            if (((GameObject)preplist[i]) == null) preplist.RemoveAt(i--);
            else
            {
                if (((GameObject)preplist[i]).GetComponent<SocialEvent>().delay != 0)
                {
                    ((GameObject)preplist[i]).GetComponent<SocialEvent>().delay -= 0.1f;
                    ((GameObject)preplist[i]).GetComponent<SocialEvent>().Location.transform.FindChild("TimeText").gameObject.GetComponent<Text>().text
                        = string.Format("{0:F2}", ((GameObject)preplist[i]).GetComponent<SocialEvent>().delay);
                    ((GameObject)preplist[i]).GetComponent<SocialEvent>().Location.transform.FindChild("PercentText").gameObject.GetComponent<Text>().text
                        = string.Format("{0:P0}", (1 - (((GameObject)preplist[i]).GetComponent<SocialEvent>().delay / 10)));
                    if (((GameObject)preplist[i]).GetComponent<SocialEvent>().delay <= 0)
                    {
                        ((GameObject)preplist[i]).GetComponent<SocialEvent>().delay = 0f;
                        GameObject tmp = (GameObject)preplist[i];
                        if (i == 0)
                        {
                            eventon = false;
                            Destroy(eventpopup);
                            eventpopup = null;
                        }
                        preplist.RemoveAt(i--);
                    }
                }
            }
        }
    }

    void checkwork()
    {
        for (int i = 0; eventlist != null && i < eventlist.Count; i++)
        {
            if (((GameObject)eventlist[i]) == null) eventlist.RemoveAt(i--);
            else
            {
                if (((GameObject)eventlist[i]).GetComponent<SocialEvent>().workTime != 0)
                {
                    ((GameObject)eventlist[i]).GetComponent<SocialEvent>().workTime -= 0.1f;
                    ((GameObject)eventlist[i]).GetComponent<SocialEvent>().Location.transform.FindChild("TimeText").gameObject.GetComponent<Text>().text
                        = string.Format("{0:F2}", ((GameObject)eventlist[i]).GetComponent<SocialEvent>().workTime);
                    ((GameObject)eventlist[i]).GetComponent<SocialEvent>().Location.transform.FindChild("PercentText").gameObject.GetComponent<Text>().text
                        = string.Format("{0:P0}", 1 - (((GameObject)eventlist[i]).GetComponent<SocialEvent>().workTime / ((GameObject)eventlist[i]).GetComponent<SocialEvent>().fullTime));
                    if (((GameObject)eventlist[i]).GetComponent<SocialEvent>().workTime <= 0)
                    {
                        ((GameObject)eventlist[i]).GetComponent<SocialEvent>().workTime = 0f;
                        GameObject.Find("PlayerManager").GetComponent<PlayerManager>().UpdateSocial((float)generate(1,0.3) * ((GameObject)eventlist[i]).GetComponent<SocialEvent>().point);
                        //accumsocial += ((GameObject)eventlist[i]).GetComponent<SocialEvent>().point;
                        GameObject tmp = (GameObject)eventlist[i];
                        WorkingSlime -= tmp.GetComponent<SocialEvent>().slimenum;
                        eventlist.RemoveAt(i--);
                    }
                }
            }
        }
    }

    void checkclick()
    {
        if (eventpopup != null && eventon)
        {
            if ((eventpopup).GetComponent<EventPop>().status == EventPop.STATUS.CANCEL)
            {
                eventon = false;
                Destroy(eventpopup);
                eventpopup = null;
                GameObject tmp = (GameObject)preplist[0];
                tmp.GetComponent<SocialEvent>().canceled = true;
                preplist.RemoveAt(0);
            }
            else if ((eventpopup).GetComponent<EventPop>().status == EventPop.STATUS.CONFIRM)
            {
                eventon = false;
                Destroy(eventpopup);
                eventpopup = null;

                if (((GameObject)preplist[0]).GetComponent<SocialEvent>().setEvent())
                {
                    eventlist.Add(preplist[0]);
                    WorkingSlime += ((GameObject)preplist[0]).GetComponent<SocialEvent>().slimenum;
                    preplist.RemoveAt(0);
                }
            }
        }
        else if (!eventon && preplist.Count != 0)
        {
            if (((GameObject)preplist[0]).GetComponent<SocialEvent>().eventname == "") preplist.RemoveAt(0);
            else if(((GameObject)preplist[0]).GetComponent<SocialEvent>().slimenum==-1) preplist.RemoveAt(0);
            else
            {
                eventpopup =
                    GetComponent<UIManager>().showEventPop(
                    fullname(getgroupname((int)(((GameObject)preplist[0]).GetComponent<SocialEvent>().groupname))) + " " + ((GameObject)preplist[0]).GetComponent<SocialEvent>().eventname,
                    socialscript((int)((GameObject)preplist[0]).GetComponent<SocialEvent>().groupname, ((GameObject)preplist[0]).GetComponent<SocialEvent>().eventname),
                    ((GameObject)preplist[0]).GetComponent<SocialEvent>().slimenum,
                    ((GameObject)preplist[0]).GetComponent<SocialEvent>().workTime);
                string tmp = ((GameObject)preplist[0]).GetComponent<SocialEvent>().eventname;
                tmp = ((GameObject)preplist[0]).GetComponent<SocialEvent>().name;
                if (eventpopup == null)
                    Debug.Log("eventpop: NULL");
                else eventon = true;
            }
        }
    }

    bool eventMaker(string Group, GameObject clone)
    {
        bool ret = true;
        int semester = GetComponent<TimeManager>().semester;
        int time = (int)GetComponent<TimeManager>().time;
        int code = getgroupcode(Group);
        int mins = 1, maxs = 3;
        string name ="";
        /*
        if (!ckfevent[0] && time == 110 && GetComponent<TimeManager>().semester % 2 == 1)
        {
            clone.GetComponent<SocialEvent>().makeEvent((groupname)code, "해맞이 축제", 5, 20);
            ckfevent[0] = true;
        }
        else if (!ckfevent[1] && time == 50 && GetComponent<TimeManager>().semester % 2 == 0)
        {
            clone.GetComponent<SocialEvent>().makeEvent((groupname)code, "포카전", 5, 20);
            ckfevent[1] = true;
        }
        else if (!ckfevent[2] && time == 160 && GetComponent<TimeManager>().semester % 2 == 0)
        {
            clone.GetComponent<SocialEvent>().makeEvent((groupname)code, "POP", 5, 20);
            ckfevent[2] = true;
        }
        */
        //else 
        if (code / 100 == 1)
        {
            if (!ckfevent[5] && time >= 150)
            {
                name = "종총";
                ckfevent[5] = true;
            }
            else if (!ckfevent[4] && time >= 95)
            {
                name = "간담회";
                maxs = 2;
                ckfevent[4] = true;
            }
            else if (!ckfevent[3] && time >= 90)
            {
                name = "중총";
                ckfevent[3] = true;
            }
            else if (!ckfevent[2] && time >= 40 && GetComponent<TimeManager>().semester % 2 == 1)
            {
                name = "대면식";
                ckfevent[2] = true;
            }
            else if (!ckfevent[1] && time >= 15)
            {
                name = "MT";
                maxs = 4;
                ckfevent[1] = true;
            }

            else if (!ckfevent[0] && time >= 5)
            {
                name = "개총";
                ckfevent[0] = true;
                print("occur");
            }
            else
            {
                System.Random r = new System.Random();
                if (r.Next(0, 100) == 0)
                {
                    name = "술자리";
                    maxs = 2;
                    Debug.Log("random");
                }
                else ret = false;
            }
        }
        else if (code / 100 == 2)
        {
            if (!ckfevent[10] && time >= 154)
            {
                name = "종총";
                ckfevent[10] = true;
            }
            else if (!ckfevent[9] && time >= 120)
            {
                name = "분반의날";
                ckfevent[9] = true;
            }
            else if (!ckfevent[8] && time >= 45 && GetComponent<TimeManager>().semester % 2 == 1)
            {
                name = "대면식";
                ckfevent[8] = true;
            }
            else if (!ckfevent[7] && time >= 25)
            {
                name = "MT";
                maxs = 4;
                ckfevent[7] = true;
            }

            else if (!ckfevent[6] && time >= 6)
            {
                name = "개총";
                ckfevent[6] = true;
            }
            else
            {
                System.Random r = new System.Random();
                if (r.Next(0, 100) == 0)
                {
                    name = "술자리";
                    maxs = 2;
                    Debug.Log("random");
                }
                else ret = false;
            }
        }
        else if (code == 301)
        {
            if (!ckfevent[15] && time >= 158)
            {
                name = "종총";
                ckfevent[15] = true;
            }
            else if (!ckfevent[14] && time >= 130 && GetComponent<TimeManager>().semester % 2 == 1)
            {
                name = "행사";
                mins = 4;
                maxs = 5;
                //time == 20?
                ckfevent[14] = true;
            }
            else if (!ckfevent[13] && time > 42 && time % 30 == 22 && ckfevent[12])
            {
                name = "술자리";
                maxs = 2;
                ckfevent[13] = true;
            }
            else if (ckfevent[13] && time % 30 == 3)
            {
                ckfevent[13] = false;
                ret =  false;
            }
            else if (!ckfevent[12] && time > 42 && time % 30 == 2)
            {
                name = "정모";
                ckfevent[12] = true;
            }
            else if (ckfevent[12] && time % 30 == 23)
            {
                ckfevent[12] = false;
                ret = false;
            }
            else if (!ckfevent[11] && time > 42)
            {
                name = "개총";
                ckfevent[11] = true;
            }
            else return false;
        }
        else if (code == 302)
        {
            if (!ckfevent[20] && time >= 158)
            {
                name = "종총";
                ckfevent[20] = true;
            }
            else if (!ckfevent[19] && time >= 140 && GetComponent<TimeManager>().semester % 2 == 1)
            {
                name = "대회";
                mins = 4;
                maxs = 5;
                //time == 20
                ckfevent[19] = true;
            }
            else if (!ckfevent[18] && time > 44 && time % 30 == 24 && ckfevent[17])
            {
                name = "술자리";
                mins = 2;
                ckfevent[18] = true;
            }
            else if (ckfevent[18] && time % 30 == 5)
            {
                ckfevent[18] = false;
                ret = false;
            }
            else if (!ckfevent[17] && time > 44 && time % 30 == 4)
            {
                name = "정모";
                ckfevent[17] = true;
            }
            else if (ckfevent[17] && time % 30 == 25)
            {
                ckfevent[17] = false;
                ret = false;
            }
            else if (!ckfevent[16] && time > 44)
            {
                name = "개총";
                ckfevent[16] = true;
            }
            else return false;
        }
        else if (code == 303)
        {
            if (!ckfevent[25] && time >= 158)
            {
                name = "종총";
                ckfevent[25] = true;
            }
            else if (!ckfevent[24] && time >= 142)
            {
                name = "대회";
                mins = 4;
                maxs = 5;
                //time == 20?
                ckfevent[24] = true;
            }
            else if (!ckfevent[23] && time > 46 && time % 30 == 26 && ckfevent[22])
            {
                name = "술자리";
                mins = 2;
                ckfevent[23] = true;
            }
            else if (ckfevent[23] && time > 46 && time % 30 == 7)
            {
                ckfevent[23] = false;
                ret = false;
            }
            else if (!ckfevent[22] && time > 46 && time % 30 == 6)
            {
                name = "정모";
                ckfevent[22] = true;
            }
            else if (ckfevent[22] && time > 46 && time % 30 == 27)
            {
                ckfevent[22] = false;
                ret = false;
            }
            else if (!ckfevent[21] && time >= 46)
            {
                name = "개총";
                ckfevent[21] = true;
            }
            else return false;
        }
        else if (code == 304)
        {
            if (!ckfevent[29] && time >= 158)
            {
                name = "종총";
                ckfevent[29] = true;
            }
            else if (!ckfevent[28] && time > 48 && time % 30 == 28 && ckfevent[27])
            {
                name = "술자리";
                mins = 2;
                ckfevent[28] = true;
            }
            else if (ckfevent[28] && time > 48 && time % 30 == 9)
            {
                ckfevent[28] = false;
                ret = false;
            }
            else if (!ckfevent[27] && time > 48 && time % 30 == 8)
            {
                name = "정모";
                ckfevent[27] = true;
            }
            else if (ckfevent[27] && time > 48 && time % 30 == 29)
            {
                ckfevent[27] = false;
                ret = false;
            }
            else if (!ckfevent[26] && time >= 48)
            {
                name = "개총";
                ckfevent[26] = true;
            }
            else return false;
        }
        else
        {
            ret = false;
        }

        if(ret) clone.GetComponent<SocialEvent>().makeEvent((groupname)code, name, mins, maxs+1, 15);
        return ret;
    }

    public bool getIn(groupname group)
    {
        if (this.group.IndexOf(group.ToString()) != -1) return false;
        Debug.Log("In");
        print(group.ToString());
        this.group.Add(group.ToString());
        return true;
    }

    public bool getOut(groupname group)
    {
        if (this.group.IndexOf(group.ToString()) == -1) return false;
        this.group.Remove(group.ToString());
        return true;
    }

    public string getgroupname(int group)
    {
        Debug.Log("getgroup" + group);
        if (group == 101) return "CSE";
        else if (group == 102) return "EEE";
        else if (group == 201) return "one";
        else if (group == 202) return "two";
        else if (group == 203) return "three";
        else if (group == 204) return "four";
        else if (group == 205) return "five";
        else if (group == 206) return "six";
        else if (group == 207) return "seven";
        else if (group == 208) return "eight";
        else if (group == 209) return "nine";
        else if (group == 210) return "ten";
        else if (group == 211) return "eleven";
        else if (group == 212) return "twelve";
        else if (group == 213) return "thirteen";
        else if (group == 214) return "fourteen";
        else if (group == 215) return "fifteen";
        else if (group == 301) return "performance";
        else if (group == 302) return "scholarship";
        else if (group == 303) return "sports";
        else if (group == 304) return "organization";
        else return "";
    }

    public int getgroupcode(string group)
    {
        if (group == "CSE") return 101;
        else if (group == "EEE") return 102;
        else if (group == "one") return 201;
        else if (group == "two") return 202;
        else if (group == "three") return 203;
        else if (group == "four") return 204;
        else if (group == "five") return 205;
        else if (group == "six") return 206;
        else if (group == "seven") return 207;
        else if (group == "eight") return 208;
        else if (group == "nine") return 209;
        else if (group == "ten") return 210;
        else if (group == "eleven") return 211;
        else if (group == "twelve") return 212;
        else if (group == "thirteen") return 213;
        else if (group == "fourteen") return 214;
        else if (group == "fifteen") return 215;
        else if (group == "performance") return 301;
        else if (group == "scholarship") return 302;
        else if (group == "sports") return 303;
        else if (group == "organization") return 304;
        else return -1;
    }

    public string fullname(string group)
    {
        if (group == "CSE") return "컴퓨터공학과";
        else if (group == "EEE") return "전자전기공학과";
        else if (group == "one") return "1분반";
        else if (group == "two") return "2분반";
        else if (group == "three") return "3분반";
        else if (group == "four") return "4분반";
        else if (group == "five") return "5분반";
        else if (group == "six") return "6분반";
        else if (group == "seven") return "7분반";
        else if (group == "eight") return "8분반";
        else if (group == "nine") return "9분반";
        else if (group == "ten") return "10분반";
        else if (group == "eleven") return "11분반";
        else if (group == "twelve") return "12분반";
        else if (group == "thirteen") return "13분반";
        else if (group == "fourteen") return "14분반";
        else if (group == "fifteen") return "15분반";
        else if (group == "performance") return "공연";
        else if (group == "scholarship") return "학술";
        else if (group == "sports") return "운동";
        else if (group == "organization") return "자치단체";
        else return "";
    }

    public void settlement()
    {
        GameObject.Find("PlayerManager").GetComponent<PlayerManager>().UpdateSocial(accumsocial);
        while (eventlist.Count != 0)
        {
            GameObject.Find("PlayerManager").GetComponent<PlayerManager>().UpdateSocial(((GameObject)eventlist[0]).GetComponent<SocialEvent>().point);
            eventlist.RemoveAt(0);
        }
    }

    string socialscript(int code, string name)
    {
        if (code / 100 == 1 || code / 100 == 2)
        {
            if (name == "개총")
                return "학기가 시작했다. 개총을 가자";
            else if (name == "MT")
            {
                if (code % 100 == 1) return "과 전체 MT. 술 마시고 죽자!";
                else return "분반 전체 MT. 술 마시고 죽자!";
            }
            else if (name == "대면식")
                return "선후배에게 얼굴도장을 찍고 술을 마시자!";
            else if (name == "중총")
                return "학기의 절반이 흘렀다! 술 마시러 가자!";
            else if (name == "간담회")
                return "학과에 평소에 하고 싶었던 말을 하러 가자!";
            else if (name == "분반의날")
                return "분반의 최대 규모 행사이다. 술 마시러 가자!";
            else if (name == "종총")
                return "학기가 끝나간다! 술 마시러 가자!";
            else if (name == "술자리")
            {
                System.Random r = new System.Random();
                int num = r.Next(1, 5);
                if (num == 1) return "친구가 깨졌다. 술 마시러 가자!";
                else if (num == 2) return "친구가 연애한다. 술 마시러 가자!";
                else if (num == 3) return "과제 끝났다. 술 마시러 가자!";
                else if (num == 4) return "술 고프다. 술 마시러 가자!";
                else if (num == 5) return "지나가는 선배를 발견, 술마시러 가자!";
                else return "";
            }
            else return "";
        }
        else if (code / 100 == 3)
        {
            if (name == "개총")
                return "원활한 동아리 활동을 위해 개총을 가자.";
            else if (name == "정모")
                return "정모날입니다 정모오세요~";
            else if (name == "술자리")
                return "정모가 끝났으니 술을 마셔야겠지?";
            else if (name == "종총")
                return "동아리 활동이 끝났다! 술 마시러 가자!";
            else if (name == "행사")
                return "동아리에서 준비해온 행사다";
            else if (name == "대회")
                return "대회에 나가서 상금을 타오자";
            else return "";
        }
        else
        {
            /*if (name == "층간담회")
                return "층에서 밥을 준다!";
            else*/
            return "";
        }
    }

    void checkappointedevent()
    {
        for (int i = 0; group != null && i < group.Count; i++)
        {
            if (eventMaker((string)group[i], clone))
            {
                preplist.Add(clone);
                clone = Instantiate(SocialEventObject);
            }
        }
    }

    void checkrandomevent()
    {
        int selection = GetComponent<UIManager>().returnNewEventSelection();
        if (selection != 0)
        {
            if (selection == 1)
            {
                System.Random r = new System.Random();
                if (group.Count == 0)
                {
                    GameObject.Find("GameManager").GetComponent<UIManager>().showMessage("Error! 가입된 그룹이 없습니다!");
                    return;
                }
                int num = r.Next(0,group.Count-1);
                GameObject tmp = clone;
                if (tmp.GetComponent<SocialEvent>().setEvent((groupname)getgroupcode((string)group[num]), "술자리", 1, 15))
                {
                    clone = Instantiate(SocialEventObject);
                    eventlist.Add(tmp);
                    WorkingSlime += tmp.GetComponent<SocialEvent>().slimenum;
                }
            }
        }
    }

    void groupUI()
    {
        ArrayList convGroup = new ArrayList();
        for(int i=0;i<group.Count;i++){
            convGroup.Add(fullname((string)group[i]));
        }
        GetComponent<UIManager>().socialList = convGroup;
    }

    bool isIn(string Group)
    {
        for(int i = 0; i < group.Count; i++)
        {
            if (((string)group[i]) == Group) return true;
        }
        return false;
    }

    double generate(double mu, double sigma)
    {
        System.Random r = new System.Random();
        const double epsilon = double.Epsilon;
        const double two_pi = 2.0 * 3.14159265358979323846;

        g = !g;

        if (!g) return z1 * sigma + mu;

        double u1, u2;
        do
        {
            u1 = r.Next(0, int.MaxValue) * (1.0 / int.MaxValue);
            u2 = r.Next(0, int.MaxValue) * (1.0 / int.MaxValue);
        } while (u1 <= epsilon);

        z0 = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Cos(two_pi * u2);
        z1 = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(two_pi * u2);
        return z0 * sigma + mu;
    }
}
