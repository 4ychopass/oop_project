﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public GameObject slimeRealloc;
    public GameObject socialLocation;
    public GameObject studyLocation;
    public GameObject Profile;
    public GameObject eventPop;
    public GameObject Talk;
    public GameObject TalkRoom;
    public GameObject Clas;
    public GameObject AClas;
    public GameObject Message;
    public GameObject MakeEvent;
    public int selectedButton;
    private int NewEventSelection;
    public int newEventSelection {
        get {
            return NewEventSelection;
        }
        set {
            NewEventSelection = value;
        }
    }
    private float KnowledgeStet;
    public float knowledgeStet {
        set {
            KnowledgeStet = value;
        }
    }
    private float KnoledgeFullStet;
    public float knowledgeFullStet {
        set {
            KnoledgeFullStet = value;
        }
    }
    private float SocialStet;
    public float socialStet {
        set {
            SocialStet = value;
        }
    }
    private float SocialFullStet;
    public float socialFullStet {
        set {
            SocialFullStet = value;
        }
    }
    private int ClassNum;
    public int classNum {
        set {
            ClassNum = value;
        }
    }
    private int SocialNum;
    public int socialNum{
        set {
            SocialNum = value;
        }    
    }
    private int WorkingSlime;
    public int workingSlime {
        set {
            WorkingSlime = value;
        }
    }
    private int RestSlime;
    public int restSlime
    {
        set
        {
            RestSlime = value;
        }
    }
    private int Semester;
    public int semester {
        set {
            Semester = value;
        }
    }
    private SocialManager.groupname Dept;
    public SocialManager.groupname dept{
        set{
            Dept = value;
        }
    }
    private ArrayList SocialList;
    public ArrayList socialList {
        set {
            SocialList = value;
        }
    }
    private ArrayList ClasList;
    public ArrayList clasList {
        set {
            ClasList = value;
        }
    }
    private ArrayList ClasHardnessList;
    public ArrayList clasHardnessList {
        set {
            ClasHardnessList = value;
        }
    }
    ArrayList studyLocations;
    ArrayList socialLocations;
    bool drawFloat_0;
    bool drawFloat_1;
    public bool DrawFloat {
        set {
            drawFloat_1 = value;
        }
    }

	// Use this for initialization
	void Start () {
        drawFloat_0 = true;
        drawFloat_1 = true;
        studyLocations = new ArrayList();
        socialLocations = new ArrayList();
        SocialList = new ArrayList();
        ClasHardnessList = new ArrayList();
        SocialStet = 0;
        KnowledgeStet = 0;
        WorkingSlime = 0;
        KnoledgeFullStet = 999;
        SocialFullStet = 999;
        ClassNum = 0;
        SocialNum = 0;
        RestSlime = 0;
    }
	void Update () {
        GameObject mainTab = GameObject.Find("Maintab");
        mainTab.transform.FindChild("uiknownum").GetComponent<Text>().text = string.Format("{0:F1}", KnowledgeStet);
        mainTab.transform.FindChild("uisocialnum").GetComponent<Text>().text = "" + (int)SocialStet;
        mainTab.transform.FindChild("uiworking").GetComponent<Text>().text = "" + WorkingSlime;
        mainTab.transform.FindChild("uirest").GetComponent<Text>().text = "" + RestSlime;
        mainTab.transform.FindChild("uisem").GetComponent<Text>().text = Semester + "학기";
        mainTab.transform.FindChild("uidept").GetComponent<Text>().text = GetComponent<SocialManager>().getgroupname((int)Dept);
        if(GameObject.FindGameObjectWithTag("Profile") != null || GameObject.FindGameObjectWithTag("SlimeAlloc") != null)
            drawFloat_0 = false;
        else
            drawFloat_0 = true;

        if (drawFloat_0 && drawFloat_1)
            GameObject.Find("Makefloat").GetComponent<Image>().enabled = true;
        else
            GameObject.Find("Makefloat").GetComponent<Image>().enabled = false;
    }

    //Realloc
    public GameObject createRealloc(bool isFirst, string classname) {
        GameObject newRealloc;
        newRealloc=(GameObject)Instantiate(slimeRealloc, new Vector3(0, 0, 1), Quaternion.identity);
        newRealloc.transform.parent = GameObject.Find("Canvas_Overlay").transform;
        newRealloc.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        newRealloc.transform.localScale = new Vector3(1, 1, 1);
        if (classname.Length > 5) {
            classname = classname.Substring(0, 5);
            classname += "...";
        }
        newRealloc.transform.FindChild("Slimealloc").FindChild("uitext").GetComponent<Text>().text = "'" + classname + "'의 재배치?";
        //newRealloc.transform.GetComponent<RectTransform>().localPosition = new Vector3(0, 0 , 1);
        if (isFirst)
        {
            newRealloc.GetComponent<ReallocScript>().noReSelectButton();
            newRealloc.transform.FindChild("Slimealloc").FindChild("uislimen").GetComponent<Button>().interactable = false;
        }
        return newRealloc;
    }
    public void destroyReallocYN() {
        Destroy(slimeRealloc);
    }

    /*
    public void goUpYN()
    {
        slimeRealloc.GetComponent<RectTransform>().localPosition += new Vector3(0, 170);
    }
    */

    //Realloc Button
    public void reSelectButton() {
        selectedButton = -1;
    }
    public void intButton1()
    {
        selectedButton = 1;
    }
    public void intButton2()
    {
        selectedButton = 2;
    }
    public void intButton3()
    {
        selectedButton = 3;
    }
    public void intButton4()
    {
        selectedButton = 4;
    }
    public void intButton5()
    {
        selectedButton = 5;
    }
    public int returnButton()
    {
        int temp = selectedButton;
        selectedButton = 0;
        return temp;
    }

    //EventLocation
    public GameObject createSocialLocation(int x, int y)
    {
        GameObject newLocation;
        newLocation = (GameObject)Instantiate(socialLocation, new Vector3(0, 0), Quaternion.identity);
        newLocation.transform.parent = GameObject.Find("Canvas").transform;
        newLocation.transform.GetComponent<RectTransform>().localPosition = new Vector3(x, y, 1);
        newLocation.transform.localScale = new Vector3(1, 1, 1);
        socialLocations.Add(newLocation);
        return newLocation;
    }
    public void destroySocialLocation()
    {
        Destroy(socialLocation);
    }
    public GameObject createStudyLocation(int x, int y)
    {
        GameObject newLocation;
        newLocation = (GameObject)Instantiate(studyLocation, new Vector3(0, 0), Quaternion.identity);
        newLocation.transform.parent = GameObject.Find("Canvas").transform;
        newLocation.transform.GetComponent<RectTransform>().localPosition = new Vector3(x, y, 1);
        newLocation.transform.localScale = new Vector3(1, 1, 1);
        studyLocations.Add(newLocation);
        //((GameObject)studyLocations[i])
        return newLocation;
    }
    public void destroyStudyLocation()
    {
        Destroy(studyLocation);
    }

    public void percentText(string _text)
    {
        socialLocation.transform.FindChild("PercentText").gameObject.GetComponent<Text>().text = _text;
    }
    public void timeText(string _text)
    {
        socialLocation.transform.FindChild("TimeText").gameObject.GetComponent<Text>().text = _text;
    }

    public void showProfile()
    {
        if (!GameObject.FindGameObjectWithTag("Profile"))
        {
            GetComponent<TimeManager>().pause();
            GameObject newProfile = (GameObject)Instantiate(Profile, new Vector3(0, 0, 0), Quaternion.identity);
            newProfile.transform.parent = GameObject.Find("Canvas_Overlay").transform;
            newProfile.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            newProfile.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            newProfile.transform.FindChild("uimajor").GetComponent<Text>().text = GetComponent<SocialManager>().fullname((string)GameObject.Find("PlayerManager").GetComponent<PlayerManager>().groupList[0]);
            newProfile.transform.FindChild("uisemestor").GetComponent<Text>().text = (int)((Semester + 1) / 2) + "학년 " + (int)((Semester + 1) % 2 + 1) + "학기";
            newProfile.transform.FindChild("uiknownum").GetComponent<Text>().text = string.Format("{0:F1}" ,KnowledgeStet);
            newProfile.transform.FindChild("uisocialnum").GetComponent<Text>().text = "" + (int)SocialStet;
            newProfile.transform.FindChild("uiknowgraph").GetComponent<RectTransform>().localScale = new Vector3(KnowledgeStet / KnoledgeFullStet, 1, 1);
            newProfile.transform.FindChild("uisocialgraph").GetComponent<RectTransform>().localScale = new Vector3(SocialStet / SocialFullStet, 1, 1);
            newProfile.transform.FindChild("uiclassnum").GetComponent<Text>().text = "" + ClassNum;
            newProfile.transform.FindChild("uiactivnum").GetComponent<Text>().text = "" + SocialNum;
            if (GameObject.Find("PlayerManager").GetComponent<PlayerManager>().playerName != null) {
                newProfile.transform.FindChild("uiplayername").GetComponent<Text>().text = GameObject.Find("PlayerManager").GetComponent<PlayerManager>().playerName;
            }
            if (GameObject.Find("PlayerManager").GetComponent<PlayerManager>().playerProfile != null) {
                Sprite playerPic = Sprite.Create(GameObject.Find("PlayerManager").GetComponent<PlayerManager>().playerProfile, new Rect(0, 0, 1040, 840), new Vector2(0, 1));
                newProfile.transform.FindChild("uiprofile").GetComponent<Image>().sprite = playerPic;
            }
        }
    }

    public GameObject showEventPop(string title, string script, int slimeNum, float time) {
        if (GameObject.FindGameObjectWithTag("eventPop") == null)
        {
            GameObject newEventPop = (GameObject)Instantiate(eventPop, new Vector3(0, 0, 0), Quaternion.identity);
            newEventPop.transform.parent = GameObject.Find("Canvas_Overlay").transform;
            newEventPop.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -594);
            newEventPop.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            newEventPop.transform.FindChild("uieventtime").GetComponent<Text>().text = "" + time;
            newEventPop.transform.FindChild("uieventslime").GetComponent<Text>().text = "" + slimeNum;
            newEventPop.transform.FindChild("uieventname").GetComponent<Text>().text = title;
            newEventPop.transform.FindChild("uieventscript").GetComponent<Text>().text = script;

            Debug.Log("eventalarm");
            return newEventPop;
        }
        else
            return null;
    }

    public GameObject showTalk() {
        if (GameObject.FindGameObjectWithTag("Talk") == null)
        {
            GameObject newTalk = (GameObject)Instantiate(Talk, new Vector3(0, 0, 0), Quaternion.identity);
            newTalk.transform.parent = GameObject.Find("Canvas_Overlay").transform;
            newTalk.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            newTalk.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            for (int i = 0; i < SocialList.Count; i++) {
                GameObject newTalkRoom = (GameObject)Instantiate(TalkRoom, new Vector3(0, 0, 0), Quaternion.identity);
                newTalkRoom.transform.parent = GameObject.Find("Canvas_Overlay").transform;
                newTalkRoom.GetComponent<RectTransform>().anchoredPosition = new Vector2(57, -227 - (170 * i));
                newTalkRoom.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                newTalkRoom.transform.parent = GameObject.FindGameObjectWithTag("Talk").transform;
                newTalkRoom.transform.FindChild("uitalkname").GetComponent<Text>().text = (string)SocialList[i];
            }
            return newTalk;
        }
        else {
            return null;
        }
    }


    public GameObject showClas()
    {
        if (GameObject.FindGameObjectWithTag("Clas") == null)
        {
            GameObject newClas = (GameObject)Instantiate(Clas, new Vector3(0, 0, 0), Quaternion.identity);
            newClas.transform.parent = GameObject.Find("Canvas_Overlay").transform;
            newClas.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            newClas.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            for (int i = 0; i < ClasList.Count; i++)
            {
                GameObject newAClas = (GameObject)Instantiate(AClas, new Vector3(0, 0, 0), Quaternion.identity);
                newAClas.transform.parent = GameObject.Find("Canvas_Overlay").transform;
                newAClas.GetComponent<RectTransform>().anchoredPosition = new Vector2(60, -359 - (170 * i));
                newAClas.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                newAClas.transform.parent = GameObject.FindGameObjectWithTag("Clas").transform;
                newAClas.transform.FindChild("uiclasname").GetComponent<Text>().text = (string)ClasList[i];
                string hardness = "";
                for (int j = 0; j < (int)ClasHardnessList[i]; j++) {
                    hardness += "★";
                }
                newAClas.transform.FindChild("uiclasload").GetComponent<Text>().text = hardness;
            }
            return newClas;
        }
        else {
            return null;
        }
    }

    public GameObject showMessage(string megString) {
        GameObject newMeg = (GameObject)Instantiate(Message, new Vector3(0, 0, 0), Quaternion.identity);
        newMeg.transform.parent = GameObject.Find("Canvas_Overlay").transform;
        newMeg.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 625);
        newMeg.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        newMeg.transform.GetComponentInChildren<Text>().text = megString;
        return newMeg;
    }

    public void makeEvent() {
        DrawFloat = false;
        NewEventSelection = 0;
        GameObject newMakeEvent = (GameObject)Instantiate(MakeEvent, new Vector3(0, 0, 0), Quaternion.identity);
        newMakeEvent.transform.parent = GameObject.Find("Canvas_Overlay").transform;
        newMakeEvent.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -1520);
        newMakeEvent.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
    }

    public int returnNewEventSelection() {
        int temp = NewEventSelection;
        NewEventSelection = 0;
        return temp;
    }
}
