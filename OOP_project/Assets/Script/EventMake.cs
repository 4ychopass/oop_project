﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EventMake : MonoBehaviour {

    public void makeEventY()
    {
        GameObject.Find("GameManager").GetComponent<UIManager>().newEventSelection = 1;
        GameObject.Find("GameManager").GetComponent<UIManager>().DrawFloat = true;
        Destroy(gameObject);
    }

    public void makeEventN() {
        GameObject.Find("GameManager").GetComponent<UIManager>().newEventSelection = -1;
        GameObject.Find("GameManager").GetComponent<UIManager>().DrawFloat = true;
        Destroy(gameObject);
    }
}
