﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    GameObject T;
    GameObject Exit;
    GameObject MT;
    private string[] titleList;
    private string[] objList;
    int index;
    // Use this for initialization
    void Start()
    {
        T = GameObject.Find("Tutorial");
        Exit = GameObject.Find("ExitButton");
        MT = GameObject.Find("mainText");
        index = 0;
        titleList = new string[7];
        titleList[0] = "튜토리얼 보기";
        titleList[1] = "1. 과목테크";
        titleList[2] = "2. 슬라임배치";
        titleList[3] = "3. 학기";
        titleList[4] = "4. 이벤트";
        titleList[5] = "5. 프로필";
        titleList[6] = "6. 엔딩";

        GameObject.Find("MainText").GetComponent<Text>().text = "1. 과목테크 / 2. 슬라임 배치\n3. 학기 / 4. 이벤트\n5.프로필 / 6. 엔딩";
        GameObject.Find("ClassTreeText").GetComponent<Text>().text = "수강신청을 원하는 과목을 선택하면 과목의 색이 바뀐다. \n 한학기에 들을 수 있는 과목은 학과마다 다르며 \n 선택을 한 후 수강신청 버튼을 누르면 \n 수강신청이 완료되고 학기가 시작된다.";
        GameObject.Find("SlimeText").GetComponent<Text>().text = "플레이어가 갖는 슬라임 수는 매 학년마다 증가한다 \nEx: 1학년: 8마리, 2학년: 13마리 ... \n 플레이어는 수강중인 과목에 슬라임을\n최대 5마리까지 배치할 수 있다. ";
        GameObject.Find("SemesterText").GetComponent<Text>().text = "게임이 진행되는 기본 단위로, 매 학기가 종료되면\n수강신청을 하게 된다.\n플레이어는 최대 10학기동안 학생생활을 할 수 있다.";
        GameObject.Find("EndingText").GetComponent<Text>().text = "학기 수와 학과 테크트리의 완료 여부, 그리고 플레이어의 지식/인맥 스탯에 따라엔딩이 정해진다 \n 엔딩을 본 후에는 게임을 다시 시작하게 되며, 플레이어가 여태까지 본 엔딩은 메인메뉴에서 확인 가능하다";
        GameObject.Find("ProfileText").GetComponent<Text>().text = "프로필화면에서는 플레이어의 스탯과 수강과목, \n참여중인 활동, 학과와 학기를 볼 수 있다. \n또한 현재 슬라임들의 정보를 볼 수 있다.";
        GameObject.Find("SocialText").GetComponent<Text>().text = "플레이어가 속한 그룹의 이벤트가 자동으로 생성되며\n개총, 종총등이 해당된다.";
        objList = new string[7];
        objList[0] = "Main";
        objList[1] = "ClassTree";
        objList[2] = "Slime";
        objList[3] = "Semester";
        objList[4] = "Social";
        objList[5] = "Profile";
        objList[6] = "Ending";

        string title = titleList[index];
        GameObject.Find("mainText").GetComponent<Text>().text = title;

        for (int i = 0; i < 7; i++)
        {
            GameObject.Find(objList[i]).transform.position = new Vector3(10, 10, 90);
        }

        GameObject N = GameObject.Find(objList[index]);
        N.transform.position = new Vector3(0, 0, 90);
    }

    public void goMain()
    {
        Application.LoadLevel("Start");
    }

    public void goNextPage()
    {
        GameObject L = GameObject.Find(objList[index]);
        index = (index + 1) % 7;
        string title = titleList[index];
        GameObject.Find("mainText").GetComponent<Text>().text = title;
        GameObject N = GameObject.Find(objList[index]);
        N.transform.position = new Vector3(0, 0, 90);
        L.transform.position = new Vector3(10, 10, 90);
        //    N.transform.position = new 

    }
    public void goPrevPage()
    {
        GameObject L = GameObject.Find(objList[index]);
        index = (index + 6) % 7;
        Debug.Log(index);
        string title = titleList[index];
        GameObject.Find("mainText").GetComponent<Text>().text = title;
        GameObject N = GameObject.Find(objList[index]);
        N.transform.position = new Vector3(0, 0, 90);
        L.transform.position = new Vector3(10, 10, 90);
    }
}
