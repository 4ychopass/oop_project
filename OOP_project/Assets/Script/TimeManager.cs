﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {
    //public variable
    public const int TIMEMAX = 180;//balance: time

    private static int Semester;
    private static float timer;
    private static bool start;
    private static bool fever;
    public bool checkStart;

    //public property
    public int semester { get { return Semester; } set { Semester = value; } }
    public float time { get { return timer; } set { timer = value; } }
    public bool semesterStart { get { return start; } 
        set {
            bool restart = start;
            start = value;
            if (!restart && start)
            {
                if (timer >= TIMEMAX)
                {
                    timer = 0f;
                    Semester++;
                    Semester = GameObject.Find("PlayerManager").GetComponent<PlayerManager>().semester;
                }
                StartCoroutine(TimeRun());
            }
        } }
    public bool fevertime { get { return fever; } }

    void Start () {
        timer = (float)TIMEMAX;
        semesterStart = false;
        GameObject.Find("PlayerManager").GetComponent<PlayerManager>().Start();
        Semester = GameObject.Find("PlayerManager").GetComponent<PlayerManager>().semester;
        fever = false;
	}

    IEnumerator TimeRun()
    {
        while (start)
        {
            timer += 0.1f;
            if((time>=TIMEMAX/2 - 30&&time<TIMEMAX/2)||(time>=TIMEMAX-30&&time<TIMEMAX)) fever = true; // balance: fever
            else fever = false;
            if (timer >= TIMEMAX)
            {
                timer = TIMEMAX;
                start = false;
                fever = false;
                GetComponent<ClassManager>().settlement();
                GetComponent<SocialManager>().settlement();
                if (GameObject.Find("PlayerManager").GetComponent<PlayerManager>().Ending() == true)
                {
                    Application.LoadLevel("endingScene");
                }
                else
                {
                    Application.LoadLevel("ClassTree");
                }
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    void Update(){
        Debug.Log(timer);
	}

    public void pause()
    {
        Time.timeScale = 0;
    }
    public void go()
    {
        Time.timeScale = 1;
    }
}
