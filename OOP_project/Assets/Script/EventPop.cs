﻿using UnityEngine;
using System.Collections;

public class EventPop : MonoBehaviour {
    public enum STATUS {
        NOTSELECT,
        CONFIRM,
        CANCEL
    };
    private STATUS Status;
    public STATUS status {
        get {
            return Status;
        }
    } 

	// Use this for initialization
	void Start () {
        Status = STATUS.NOTSELECT;
	}

    public void Cancel() {
        Status = STATUS.CANCEL;
    }

    public void Confirm() {
        Status = STATUS.CONFIRM;
    }
}
