﻿using System;
using System.IO;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ClassManager : MonoBehaviour {
    ArrayList course;//수강신청과목 from SignedClassList
    ArrayList accumpoint;//과목별 누적 point
    bool end;//유효시간 판별, 초기 수강신청과목 받아오기 체크용
    ArrayList Locationset;//study location UI 관리용
    ArrayList messageset;//slime allocation UI 관리용 
    float point;//no use for now
    ArrayList eventqueue;//class event가 UI상으로 보이는 순대로 관리하는 queue
    bool eventon;//현재 수행가능한 event가 있는지 체크
    bool semester;//TimeManager의 semesterstart 변화 확인
    GameObject a;//to make null gameobject
    ArrayList curnum;

    private int WorkingSlime;
    public int workingSlime { get { return WorkingSlime; } }

    static double z0, z1;
    static bool g;

    void Start()
    {
        course = new ArrayList();
        end = true;
        Locationset = new ArrayList();
        messageset = new ArrayList();
        eventqueue = new ArrayList();
        curnum = new ArrayList();
        eventon = false;
        a = new GameObject();
        semester = false;
        WorkingSlime = 0;
    }

    IEnumerator Calc()
    {
        while (semester)
        {
        //calculate time 
	    //check all list and reduce left time
            for (int i = 0; course != null && i < course.Count; i++)
            {
                Debug.Log("Time Left : " +((DataClassNode)course[i]).TimeLeft);
                if (((DataClassNode)course[i]).TimeLeft > 0)
                {
                    ((DataClassNode)course[i]).TimeLeft -= 0.1f;

                    //UI
                    ((GameObject)Locationset[i]).transform.FindChild("TimeText").gameObject.GetComponent<Text>().text 
                        = string.Format("{0:F2}",((DataClassNode)course[i]).TimeLeft);
                    ((GameObject)Locationset[i]).transform.FindChild("PercentText").gameObject.GetComponent<Text>().text
                        = string.Format("{0:P0}",(1-(((DataClassNode)course[i]).TimeLeft)/10));

                    if (((DataClassNode)course[i]).TimeLeft <= 0)//check end
                    {
                        WorkingSlime -= ((DataClassNode)course[i]).CurrentSlime;
                        ((DataClassNode)course[i]).TimeLeft = 0f;
                        ((DataClassNode)course[i]).CurrentSlime = 0;
                        GameObject tmp = (GameObject)Locationset[i];
                        eventqueue.Add(i);
                        Locationset[i] = Instantiate(a);
                        Destroy(tmp);
                    }

                    //accum point
                    float p = slimescale(((DataClassNode)course[i]).CurrentSlime);
                    p *= (float)generate(1.0, 0.3);
                    if (GetComponent<TimeManager>().fevertime) p *= 2;
                    accumpoint[i] = (float)(accumpoint[i]) + p;//point calc
                }
            }
            yield return new WaitForSeconds(0.1f);//make match with TimeManager
        }//end while
    }//end couroutine

    void Update()
    {
        //check for pause
        if (semester == false && GetComponent<TimeManager>().semesterStart == true)
        {
            semester = GetComponent<TimeManager>().semesterStart;
            StartCoroutine(Calc());
        }
        else
        {
            semester = GetComponent<TimeManager>().semesterStart;
        }

        //initialize for semestar start //or load
        if (end&&semester)
        {
            //initialize course
            course = new ArrayList();
            course = GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_list;
            classUI();
            //initialize Locationset, messageset, eventqueue
            for (int i = 0; i < course.Count; i++)
            {
                ((DataClassNode)course[i]).TimeLeft = 0f;
                Locationset.Add(Instantiate(a));
                messageset.Add(Instantiate(a));
                eventqueue.Add(i);
                curnum.Add(-1);
            }

            //initualize accumpoint
            accumpoint = new ArrayList();
            for (int i = 0; course != null && i < course.Count; i++)
            {
                accumpoint.Add(0f);
            }
            end = false;
        }//init

        reallocUI();
    }

    public bool setstudy(string subject, int num)
    {
        bool ret = false;
        for (int i = 0; i < course.Count; i++)
        {
            if (((DataClassNode)course[i]).Classname == subject)
            {
                ret = true;
                /*if (((DataClassNode)course[i]).MaxSlime < num)
                {
                    GetComponent<UIManager>().showMessage("배치 가능한 슬라임 수를 초과하였습니다.");
                    return false;
                }*/
                ret = GetComponent<SlimeManager>().setSlimeWork(SlimeManager.DESTLIST.DESTINATION_SCI_BLD, num, 10);
                if (!ret)
                {
                    GetComponent<UIManager>().showMessage("슬라임의 개수가 부족합니다.");
                    return false;
                }

                ((DataClassNode)course[i]).CurrentSlime = num;
                ((DataClassNode)course[i]).TimeLeft = 10f;
                GameObject tmp = (GameObject)Locationset[i];
                Locationset[i] = GetComponent<UIManager>().createStudyLocation(-174, -189);
                Destroy(tmp);
                break;
            }
        }
        return ret;
    }

    float grade(float result,int level)//Balance
    {
        if (level == 1)
        {
            if (result < 32)
                return 0f;
            else if (result < 35)
                return 0.7f;
            else if (result < 37)
                return 1f;
            else if (result < 39)
                return 1.3f;
            else if (result < 42)
                return 1.7f;
            else if (result < 44)
                return 2f;
            else if (result < 46)
                return 2.3f;
            else if (result < 49)
                return 2.7f;
            else if (result < 51)
                return 3f;
            else if (result < 53)
                return 3.3f;
            else if (result < 56)
                return 3.7f;
            else if (result < 58)
                return 4.0f;
            else return 4.3f;
        }
        else if (level == 3)
        {
            if (result < 40)
                return 0f;
            else if (result < 43)
                return 0.7f;
            else if (result < 45)
                return 1f;
            else if (result < 47)
                return 1.3f;
            else if (result < 50)
                return 1.7f;
            else if (result < 52)
                return 2f;
            else if (result < 54)
                return 2.3f;
            else if (result < 57)
                return 2.7f;
            else if (result < 59)
                return 3f;
            else if (result < 61)
                return 3.3f;
            else if (result < 64)
                return 3.7f;
            else if (result < 66)
                return 4.0f;
            else return 4.3f;
        }
        else if (level == 4)
        {
            if (result < 47)
                return 0f;
            else if (result < 50)
                return 0.7f;
            else if (result < 52)
                return 1f;
            else if (result < 54)
                return 1.3f;
            else if (result < 57)
                return 1.7f;
            else if (result < 59)
                return 2f;
            else if (result < 61)
                return 2.3f;
            else if (result < 64)
                return 2.7f;
            else if (result < 66)
                return 3f;
            else if (result < 68)
                return 3.3f;
            else if (result < 71)
                return 3.7f;
            else if (result < 73)
                return 4.0f;
            else return 4.3f;
        }
        else if (level == 5)
        {
            if (result < 53)
                return 0f;
            else if (result < 56)
                return 0.7f;
            else if (result < 58)
                return 1f;
            else if (result < 60)
                return 1.3f;
            else if (result < 63)
                return 1.7f;
            else if (result < 65)
                return 2f;
            else if (result < 67)
                return 2.3f;
            else if (result < 70)
                return 2.7f;
            else if (result < 72)
                return 3f;
            else if (result < 74)
                return 3.3f;
            else if (result < 77)
                return 3.7f;
            else if (result < 79)
                return 4.0f;
            else return 4.3f;
        }
        else return 0;
    }

    float slimescale(int slimenum)
    {
        if (slimenum == 1) return 1f;
        else if (slimenum == 2) return 1.5f;
        else if (slimenum == 3) return 2f;
        else if (slimenum == 4) return 2.5f;
        else if (slimenum == 5) return 3f;
        else return 0;
    }

    public void settlement()
    {
        for (int i = 0; i < course.Count; i++)
            {
                ((DataClassNode)course[i]).isdone = true;
                ((DataClassNode)course[i]).Grade = grade((float)Math.Sqrt((float)accumpoint[i]),((DataClassNode)course[i]).level);
                float p = 2 * GameObject.Find("PlayerManager").GetComponent<PlayerManager>().gpa;
                if (p == 0) p = 6.0f;
                GameObject.Find("PlayerManager").GetComponent<PlayerManager>().UpdateInteligence( (((DataClassNode)course[i]).level+1),
                    grade((float)Math.Sqrt((float)accumpoint[i]) + p, ((DataClassNode)course[i]).level));
                //각 node 별로 학점을 부여.
            }
            eventqueue.Clear();
            while (Locationset.Count != 0)
            {
                Destroy((GameObject)Locationset[0]);
                Locationset.RemoveAt(0);
            }
            while (messageset.Count != 0)
            {
                Destroy((GameObject)messageset[0]);
                messageset.RemoveAt(0);
            }
            eventon = false;
            end = true;
            semester = false;
    }

    //UI for class list
    void classUI()
    {
        ArrayList classname = new ArrayList();
        ArrayList classlevel = new ArrayList();
        for (int i = 0; i < course.Count; i++)
        {
            classname.Add(((DataClassNode)course[i]).Classname);
            classlevel.Add(((DataClassNode)course[i]).level);
        }
        GetComponent<UIManager>().clasList = classname;
        GetComponent<UIManager>().clasHardnessList = classlevel; 
    }

    //
    void reallocUI()
    {
        	//update UI
        if (semester)
        {
            if (!eventon && eventqueue.Count != 0)
            {
                GameObject tmp = (GameObject)messageset[(int)eventqueue[0]];
                messageset[(int)eventqueue[0]] = GetComponent<UIManager>().createRealloc((float)accumpoint[(int)eventqueue[0]] == 0,((DataClassNode)course[(int)eventqueue[0]]).Classname);
                Destroy(tmp);
                eventon = true;
            }
            else if (eventon)
            {
                if (GetComponent<UIManager>().selectedButton != 0)
                {
                    if (GetComponent<UIManager>().selectedButton != -1) curnum[(int)eventqueue[0]] = GetComponent<UIManager>().selectedButton;
                    bool act = setstudy(((DataClassNode)course[(int)eventqueue[0]]).Classname, (int)curnum[(int)eventqueue[0]]);
                    if (act)
                    {
                        GameObject tmp = (GameObject)messageset[(int)eventqueue[0]];
                        WorkingSlime += (int)curnum[(int)eventqueue[0]];
                        messageset[(int)eventqueue[0]] = Instantiate(a);
                        Destroy(tmp);
                        eventqueue.RemoveAt(0);
                        eventon = false;
                    }
                    GetComponent<UIManager>().returnButton();
                }
            }
        }
    }

    double generate(double mu, double sigma)
    {
        System.Random r = new System.Random();
        const double epsilon = double.Epsilon;
        const double two_pi = 2.0 * 3.14159265358979323846;

        g = !g;

        if (!g) return z1 * sigma + mu;

        double u1, u2;
        do
        {
            u1 = r.Next(0, int.MaxValue) * (1.0 / int.MaxValue);
            u2 = r.Next(0, int.MaxValue) * (1.0 / int.MaxValue);
        } while (u1 <= epsilon);

        z0 = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Cos(two_pi * u2);
        z1 = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(two_pi * u2);
        return z0 * sigma + mu;
    }
}
