﻿using UnityEngine;
using System.Collections;

public class Message : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(showMessage());
	}

    IEnumerator showMessage() {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
