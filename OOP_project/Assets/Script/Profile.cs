﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class Profile : MonoBehaviour {

    public void showTalks() {
        GameObject.Find("GameManager").GetComponent<UIManager>().showTalk();
    }

    public void showClasList() {
        GameObject.Find("GameManager").GetComponent<UIManager>().showClas();
    }

    public void showAchievement() {
        Social.ShowAchievementsUI();
    }
}
