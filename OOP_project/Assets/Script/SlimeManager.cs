﻿using UnityEngine;
using System.Collections;

public class SlimeManager : MonoBehaviour {

    public enum DESTLIST {
        DESTINATION_DORM,
        DESTINATION_SCI_BLD,
        DESTINATION_HJ_MRK
    };

    public Area[] destinationList;

    public GameObject originSlime;

    private ArrayList slimeList;

    private int SlimeNum;

    public int slimeNum {
        get {
            return SlimeNum;
        }
    }

    private int AvailSlimeNum;

    public int availSlimeNum {
        get {
            return AvailSlimeNum;
        }
    }    

	// Use this for initialization
	void Start () {
        //SlimeNum = 0;
        AvailSlimeNum = 0;
        slimeList = new ArrayList();
	}
	
	// Update is called once per frame
	void Update () {
        AvailSlimeNum = 0;
        for (int i = 0; i < slimeList.Count; i++) {
            if (((GameObject)slimeList[i]).GetComponentInChildren<Slime>().CurrentArea == destinationList[0]) {
                AvailSlimeNum++;
            }
        }

        if (SlimeNum > slimeList.Count) {
            int difference = slimeNum - slimeList.Count;
            for (int i = 0; i < difference; i++) {
                GameObject newSlime = (GameObject)Instantiate(originSlime, destinationList[0].gameObject.transform.position+(Vector3)Random.insideUnitCircle, Quaternion.identity);
                slimeList.Add(newSlime);
                AvailSlimeNum++;
            }
        }
	}

    public void addSlimeNum(int addNum) {
        SlimeNum += addNum;
    }

    public bool setSlimeWork(DESTLIST setArea, int workNum, float workTime) {
        if (AvailSlimeNum >= workNum)
        {
            for (int i = 0; i < slimeList.Count && workNum > 0; i++) {
                if (((GameObject)slimeList[i]).GetComponentInChildren<Slime>().destination == destinationList[0]) {
                    switch (setArea) {
                        case DESTLIST.DESTINATION_DORM:
                            ((GameObject)slimeList[i]).GetComponentInChildren<Slime>().setSlimeWork(destinationList[0], workTime);
                            break;

                        case DESTLIST.DESTINATION_SCI_BLD:
                            ((GameObject)slimeList[i]).GetComponentInChildren<Slime>().setSlimeWork(destinationList[1], workTime);
                            break;

                        case DESTLIST.DESTINATION_HJ_MRK:
                            ((GameObject)slimeList[i]).GetComponentInChildren<Slime>().setSlimeWork(destinationList[2], workTime);
                            break;
                    }
                    AvailSlimeNum--;
                    workNum--;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}
