﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System;


[Serializable]
public class ClassNode : MonoBehaviour {

    public int Maxpoint;
    public int MaxSlime;
    public int level;
    public int CurrentSlime;
    public float TimeLeft;
    public float Grade;
    public float RunTime;
    public string Classname;
    public int num;
    public bool isdone;
    public bool isselect;
    public ArrayList child;
    public ArrayList parent;
    //과목이 가져야하는 기본적인 정보들
    public float x;
    public float y;
    //과목을 화면에 보여줄 위치
    Camera _mainCam = null;
    private bool _mouseState;
    private GameObject target;
    //선택된 게임오브젝트
    public int childnum;
    public int parentnum;
    //부모 자식의 개수
    private int flag;

    TextAsset info;
	// Use this for initialization
    public void Start () {

        _mainCam = Camera.main;

        if(child == null)
            child = new ArrayList();

        if(parent == null)
            parent = new ArrayList();
	//초기화 한다.
	}
	
	// Update is called once per frame
	void Update () {
        bool mousechange = false;//마우스의 상태가 변경되었는가에 대한 변수 이것이 없다면 계속 해서 누르고 있을때 빠르게 루프가 한변 더 돌아 이용하기 불편하다.
        GameObject background = GameObject.Find("ClassManager");
        GameObject classtree = GameObject.Find("ClassTreeManager");
        GameObject name = transform.Find("NodeName(Clone)").gameObject;
        GameObject Level = transform.Find("ClassLevel(Clone)").gameObject;
        //기본 정보들을 불러온다.
        transform.position = new Vector3(x, y, 0)+background.transform.position;//위치설정
        name.GetComponent<TextMesh>().text = Classname;//과목이름 설정
        name.GetComponent<TextMesh>().characterSize = 0.4f;//과목이름의 글자사이즈 설정
        name.GetComponent<Transform>().transform.position = new Vector3(x+1.25f, y-0.35f, 0) + background.transform.position;//과목이름을 표시할 위치설정
        Level.GetComponent<TextMesh>().characterSize = 0.2f;//난이도를 표시할 글자시이즈 설정
        Level.GetComponent<TextMesh>().text = "Level : ";//난이도의 기본형태
        for (int i=0; i < level; i++)//난이도아 맞추어 별을 추가한다.
        {
            Level.GetComponent<TextMesh>().text = Level.GetComponent<TextMesh>().text + "★";
        }
        Level.GetComponent<Transform>().transform.position = new Vector3(x , y+0.2f , 0) + background.transform.position;//난이도를 표시할 위치 설정
        float stringlen = (float)Classname.Length;
        GetComponent<BoxCollider>().size = new Vector3(2.5f, 0.9f, 0);//선택 영열을 설정한다.
        GetComponent<BoxCollider>().center = new Vector3(1.25f, -0.25f, 0);//선택 영역을 설정한다.
        GameObject back;//뒷 이미지를 설정한다.
        if (((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isdone)//완료한 과목일 경우
        {
            back = transform.Find("NodeBackground(Clone)").gameObject;//배경은 완료한 과목의 이미지이다.
            transform.Find("NodeUndo(Clone)").gameObject.SetActive(false);//다른 이미지는 비활성화 한다.
        }
        else
        {
            back = transform.Find("NodeUndo(Clone)").gameObject;//수강하지 않은 과목의 이미지이다.
            transform.Find("NodeBackground(Clone)").gameObject.SetActive(false);//수강한 과목의 이미지는 비활성화 한다.
        }
            
        
        back.GetComponent<Transform>().position = new Vector3(1.25f, -0.25f, 0) + transform.position;
        back.GetComponent<Transform>().localScale = new Vector3(2.5f, 0.9f, 1);
        //배경 이미지의 위치를 설정한다.
        if (((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isselect)//선택된 과목
        {
            GameObject check = transform.Find("NodeCheck(Clone)").gameObject;
            check.transform.position = new Vector3(0.1f, 0f, 0) + transform.position;//체크 표시의 위치를 지정한한.
            name.GetComponent<TextMesh>().color = new Color(0.8f, 0.8f, 0.8f);//글자색을 조정한다.

        }

        else if (((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isdone)//수강완료한 과목
        {
            GameObject check = transform.Find("NodeCheck(Clone)").gameObject;
            check.SetActive(false);//체크표시는 비활성화한다.
            name.GetComponent<TextMesh>().color = new Color(0.8f, 0.8f, 0.8f);//글자색을 조정한다.
        }
        else if (sign_avle())//수강가능한 과목
        {
            GameObject check = transform.Find("NodeCheck(Clone)").gameObject;
            check.transform.position = new Vector3(1f, -0.25f, 10f) + transform.position;//.체크표시를 배경이미지 뒤로 보내 보이지 않도록 한다.
            name.GetComponent<TextMesh>().color = new Color(0f, 1f, 0.3f);//글자색을 지정한다.
        }
        else if (!((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isdone)//그 이외의 과목들
        {
            GameObject check = transform.Find("NodeCheck(Clone)").gameObject;
            check.transform.position = new Vector3(1f, -0.25f, 10f) + transform.position;//체크표시를 배경이미지 뒤로 보내 보이지 않게한다.(비활성화하면 다시 생성이 안되서 비활성화는 안된다ㅏ)
            name.GetComponent<TextMesh>().color = new Color(1f, 1f, 1f);//글자색을 지정한다.
        }
        if (true == Input.GetMouseButtonDown(0))//과목을 클릭하면
        {
            if (GetClickedObject() != null)//클릭된 오브젝트가 있을때
            {
                target = GetClickedObject();//클릭된 오브젝트를 받아온다.
               
                if (target.Equals(gameObject))//클릭된 오브젝트가 자신일때
                {
                    if(sign_avle())//선택할수 있는 과목이면
                    {
                        if (_mouseState == false) mousechange = true;//마우스가 변했다.
                        _mouseState = true;//마우스 상태를 변경한다.
                    }
                    /*else if (num == 0)
                    {
                        if (_mouseState == false) mousechange = true;
                        _mouseState = true;
                    }*/
                }
            }

        }
        else if (Input.GetMouseButtonUp(0))//클릭되지 않았을때
        {
            if (_mouseState == true) mousechange = true;//마우스가 클릭된 상태였다면 마우스가 변했다.
            _mouseState = false;//마우스 상태를 클릭되지 않음으로 변경한다.
        }

        if(_mouseState==true && mousechange==true)//마우스가 클릭되고 마우스 변경이 있을때
        {
            GameObject SelectedClassList = GameObject.Find("SignedClassList");
            flag = 0;
            for(int i=0;i< SelectedClassList.GetComponent<SignedClassList>().list.Count; i++)
            {
                if((ClassNode)SelectedClassList.GetComponent<SignedClassList>().list[i] == this) { flag = 1; }
            }//수강목록에 넣는다.
            GameObject infomation = GameObject.Find("Text");//텍스트를 찾는다. 여기에 과목의 정보를 표시한다.
            info = Resources.Load(GetComponent<ClassNode>().Classname) as TextAsset;//과목정보의 파일이름은 과목이름과 파일명이 같다.
            if (info != null)
            {
                StringReader sr = new StringReader(info.text);//과목정보를 읽어온다.
                infomation.GetComponent<Text>().text = sr.ReadToEnd();//텍스트에 과목정보를 그대로 붙여넣는다.
            }
            else//정보가 없다면 빈 텍스트로 남겨둔다.
            {
                infomation.GetComponent<Text>().text = "";
            }
            if (flag == 0)//수강신청되지 않은 과목이라면 신청한다..
            {
                if (!((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isselect)//선택되지 않은과목이면
                {
                    SelectedClassList.GetComponent<SignedClassList>().list.Add(GetComponent<ClassNode>());
                    ((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isselect = true;
                }
            }
            else if (flag == 1)//수강신청된 과목이면 뺀다.
             {         
                 if (((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isselect)//선택된 과목이면
                 {
                    if (delete_avle())//지울수 있는 과목일 경우
                    {
                        SelectedClassList.GetComponent<SignedClassList>().list.Remove(GetComponent<ClassNode>());//리스트에서 지운다.
                        ((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isselect = false;
                    }
                 }
             }

        }

    }

    public void setdata(ClassNode a)//데이터 설정
    {
        Maxpoint = a.Maxpoint;
        MaxSlime = a.MaxSlime;
        childnum = a.childnum;
        parentnum = a.parentnum;
        Classname = a.Classname;
        num = a.num;
        isdone = a.isdone;
        child = a.child;
        parent = a.parent;
        RunTime = a.RunTime;
        level = a.level;

        x = a.x;
        y = a.y;
    }

    private GameObject GetClickedObject()//클릭된 오브젝트를 반환한다.
    {
        RaycastHit hit;

        GameObject result = null;

        Ray ray = _mainCam.ScreenPointToRay(Input.mousePosition);//메인카메라에서 ray를 쏜다.

        if (Physics.Raycast(ray.origin, ray.direction * 10, out hit))//클릭이 되면
        {
            result = hit.collider.gameObject;//광선에 맞은 오브젝트
        }
        return result;//반환한다.
    }


    private bool sign_avle()//수강할수 있는 과목
    {
        GameObject classtree = GameObject.Find("ClassTreeManager");
        if (((ClassNode)(classtree.GetComponent<ClassTreeManager>().Class[num])).isdone) return false;//이미 수강한 과목은 안된다.
        for(int i=0;i<parent.Count;i++)//모든 부모과목이
        {
            if (((ClassNode)parent[i]).isdone == false && ((ClassNode)parent[i]).isselect == false) return false;//하나라도 수강하지 않았다면 수강할수 없다.
        }//수강완료되었거나 수강신청중이면 수강할 수 있다.
        return true;//여기까지 무사히 왔다면 수강할 수 있다.
    }
    private bool delete_avle()//지울수 있는가?
    {
        for (int i = 0; i < child.Count; i++)//자식들중 수강신청된 과목이 하나도 없을때 수강목록에서 뺄 수 있다.
        {
            if (((ClassNode)child[i]).isdone == true || ((ClassNode)child[i]).isselect == true) return false;
        }
        return true;
    }

    public void setdataclass(DataClassNode a)
    {
        Maxpoint = a.Maxpoint;
        MaxSlime = a.MaxSlime;
        childnum = a.childnum;
        parentnum = a.parentnum;
        Classname = a.Classname;
        num = a.num;
        isdone = a.isdone;
        child = new ArrayList();
        parent = new ArrayList();
        RunTime = a.RunTime;
        level = a.level;

        x = 0;
        y = 0;

    }
}
