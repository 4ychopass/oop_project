﻿using UnityEngine;
using System.Collections;

public class RoadArea : Area {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    override
    public bool isDestination() {
        return false;
    }

    override
    public Area nextArea(Area searchArea, DestArea destination)
    {
        for (int i = 0; i < connArea.Length; i++)
        {
            if (connArea[i] == searchArea)
                continue;

            if (connArea[i].nextArea(this, destination) != null)
                return connArea[i];
        }
        return null;
    }
}
