﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Scroll : MonoBehaviour {
    Vector3 originpos;
	// Use this for initialization
	void Start() {
        originpos=transform.position;

    }
	
	// Update is called once per frame
	void Update () {

    }

    public void moveleft()//왼쪽으로 움직인다.
    {
        //GameObject view;
        //view = GameObject.Find("ClassManager");
        transform.position = new Vector3(1, 0, 0) + transform.position;//원래위치에서 왼쪽으로 한칸 움직인다.
    }

    public void moveright()//오른쪽으로 움직인다.
    {
       // GameObject view;
        //view = GameObject.Find("ClassManager");
        transform.position = new Vector3(-1, 0, 0) + transform.position;//원래 위치에서 오른족으로 한칸 움직인다.
    }

    public void move()//스크롤에 의해 움직인다.
    {
        GameObject scrollbar;
        scrollbar = GameObject.Find("Scrollbar");//스크롤바로 부터 정보를 받아온다.
        transform.position = new Vector3(scrollbar.GetComponent<Scrollbar>().value * 12, 0, -10f);//스크롤의 값을 받아서 위치를 조정한다.

    }
}
