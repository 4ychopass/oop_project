﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System;

[Serializable]
public class DataClassNode{

    public int Maxpoint;
    public int MaxSlime;
    public int level;
    public int CurrentSlime;
    public float TimeLeft=0;
    public float Grade;
    public float RunTime;
    public string Classname;
    public int num;
    public bool isdone;
    public bool isselect;
    public int childnum;
    public int parentnum;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setdata(ClassNode a)
    {
        Maxpoint = a.Maxpoint;
        MaxSlime = a.MaxSlime;
        Classname = a.Classname;
        num = a.num;
        isdone = a.isdone;
        RunTime = a.RunTime;
        level = a.level;
        childnum = a.childnum;
        parentnum = a.parentnum;

    }
}
