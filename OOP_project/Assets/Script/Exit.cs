﻿using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour {
    public void exit() {
        GameObject.Find("GameManager").GetComponent<TimeManager>().go();
        Destroy(gameObject);
    }

    public void exitNoGo()
    {
        Destroy(gameObject);
    }
}
