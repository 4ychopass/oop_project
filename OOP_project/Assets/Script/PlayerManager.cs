﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Reflection;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;


[Serializable]
public class PlayerManager : MonoBehaviour
{

    public ArrayList ClassList;     // 수강과목, 그룹, 이벤트를 실시간으로 저장하는 arraylist
    public ArrayList groupList = new ArrayList();
    public ArrayList eventList;
    public ArrayList prepList;
    public ArrayList oldClassList;

    private int classNum;           // list.count
    private int groupNum;
    private int eventNum;
    private int prepNum;

    public ArrayList oldEventList;  // 여태까지 발생한 이벤트 리스트를 저장

    private Hashtable dataTable;    // 플레이어의 정보를 저장할 hashtable
    private object tempList;        // 저장하는 과정에서 dataTable->tempList로 cast
    private object obj;             // 불러올때 obj에 저장->dataTable로 옮김
    private GameObject GameManager; // InGame Scene의 GamaManager를 찾아서 위치를 저장해둠 
    public GameObject ExitDialog;

    public string endingName;       // 엔딩 제목과 내용을 출력하기 위한 string변수
    public string endingContent;

    private int flag;
    static bool isFirstTime = true;
    private int CurCredit;
    public int curcredit
    {
        get
        {
            return CurCredit;
        }
        set
        {
            CurCredit = value;
        }
    }
    private int OldCredit;
    public int oldcredit
    {
        get
        {
            return OldCredit;
        }
        set
        {
            OldCredit = value;
        }
    }
    private float InteligenceStat;    // 지식 stat
    public float inteligenceStat
    {
        get
        {
            return InteligenceStat;
        }
    }
    private float GPA;
    public float gpa
    {
        get
        {
            return GPA;
        }
    }
    private float SocialStat;         // 인맥 stat
    public float socialStat
    {
        get
        {
            return SocialStat;
        }
    }
    private int SlimeNum;           // 현재 slimenum
    public int slimeNum
    {
        get
        {
            return SlimeNum;
        }
    }
    private int Semester;           // 학기수
    private int _semester;
    public int semester
    {
        get
        {
            return Semester;
        }
    }
    public float Time;

    private string PlayerName;
    public string playerName {
        get {
            return PlayerName;
        }
    }

    private Texture2D PlayerProfile;
    public Texture2D playerProfile {
        get {
            return PlayerProfile;
        }
    }
    

    public enum SLIMENUMS
    {      // 1년이 지날 때 마다 증가함
        NUM_ONE = 8,
        NUM_TWO = 13,
        NUM_THREE = 18,
        NUM_FOUR =23,
        NUM_FIVE = 25
    };
    // Use this for initialization
    public void Start()
    {
        if (isFirstTime)
        {
            PlayerName = null;
            PlayerProfile = null;
            isFirstTime = false;
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            // recommended for debugging:
            //PlayGamesPlatform.DebugLogEnabled = true;
            // Activate the Google Play Games platform
            PlayGamesPlatform.Activate();
            
            
        }
        if (Application.loadedLevel == 0) {
            Social.localUser.Authenticate((bool success) => {
                PlayerName = ((PlayGamesLocalUser)Social.localUser).userName;
                StartCoroutine(getImage());
                // handle success or failure
            });
        }


        //PlayerName = "김수환";
        string path = pathForDocumentsFile("savedata.dat");
        Debug.Log("P start!");
        GameObject PM = GameObject.Find("PlayerManager");
        DontDestroyOnLoad(PM);
        if (Application.loadedLevelName == "Start")             //맨 처음 Start Scene에서 start되었을 때
            {
            if (!File.Exists(path))        // 세이브 파일이 없으면 load버튼 제거
                Destroy(GameObject.Find("LoadButton"));

            //초기화 과정
            dataTable = new Hashtable(); 

            InteligenceStat = 0;
            SocialStat = 0;
            SlimeNum = 0;

            Semester = 0;

            classNum = 0;
            groupNum = 0;
            eventNum = 0;
            Time = 0f;

            CurCredit = 0;
            OldCredit = 0;
            GPA = 0;

            ClassList = new ArrayList();
            oldEventList = new ArrayList();
            eventList = new ArrayList();
            prepList = new ArrayList();
        }
        else if (Application.loadedLevelName == "InGame")
        {
            // InGameScene이면 semester값 +1
            Semester++;
            GameManager = GameObject.Find("GameManager");
            

            if (Semester == 1 || Semester == 2)
            {
                if (Semester == 1) {
                    Social.ReportProgress("CgkIy-LC4v0OEAIQAQ", 100.0f, (bool success) => {
                        // handle success or failure
                    });
                }
                GameManager.GetComponent<SlimeManager>().addSlimeNum((int)SLIMENUMS.NUM_ONE);
                SlimeNum = (int)SLIMENUMS.NUM_ONE;
            }
            else if (Semester == 3 || Semester == 4)
            {
                GameManager.GetComponent<SlimeManager>().addSlimeNum((int)SLIMENUMS.NUM_TWO);
                SlimeNum = (int)SLIMENUMS.NUM_TWO;
            }
            else if (Semester == 5 || Semester == 6)
            {
                GameManager.GetComponent<SlimeManager>().addSlimeNum((int)SLIMENUMS.NUM_THREE);
                SlimeNum = (int)SLIMENUMS.NUM_THREE;
            }
            else if (Semester == 7 || Semester == 8)
            {
                GameManager.GetComponent<SlimeManager>().addSlimeNum((int)SLIMENUMS.NUM_FOUR);
                SlimeNum = (int)SLIMENUMS.NUM_FOUR;
            }
            else if (Semester == 9 || Semester == 10)
            {
                GameManager.GetComponent<SlimeManager>().addSlimeNum((int)SLIMENUMS.NUM_FIVE);
                SlimeNum = (int)SLIMENUMS.NUM_FIVE;
            }

            //semester ui 등록
            try
            {
                GameObject.Find("GameManager").GetComponent<UIManager>().semester = Semester;
            }
            catch (NullReferenceException e) { }

            // 새로운 classList 
            UpdateClassList();              // 수강신청을 받아옴
                                            // Debug.Log("check 123");
            CurCredit = 0;
            for(int i = 0; i < ClassList.Count; i++)
            {
                CurCredit += (((DataClassNode)ClassList[i]).level + 1);
            }
            OldCredit = 0;
            for(int i=0; i < GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_oldList.Count-ClassList.Count; i++)
            {
                OldCredit += (((DataClassNode)GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_oldList[i]).level + 1);
            }
            if (OldCredit != 0) GPA = InteligenceStat / OldCredit;
            else GPA = 0;
            GameManager.GetComponent<TimeManager>().semesterStart = true;       // 시간이 흐르도록 설정후
            Time = GameManager.GetComponent<TimeManager>().time;                // 흐른 시간을 받아와서 저장
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && GameObject.FindGameObjectWithTag("ExitDialog") == null) {
            GameObject exitDialog = (GameObject)Instantiate(ExitDialog, new Vector3(0, 0, 0), Quaternion.identity);
            if (GameObject.Find("Canvas_Overlay") != null)
            {
                exitDialog.transform.parent = GameObject.Find("Canvas_Overlay").transform;
                exitDialog.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
                exitDialog.GetComponent<RectTransform>().localScale = new Vector3(3.375528f, 3.375528f, 3.375528f);
            }
            else if (GameObject.Find("Canvas") != null) {
                exitDialog.transform.parent = GameObject.Find("Canvas").transform;
                exitDialog.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
                exitDialog.GetComponent<RectTransform>().localScale = new Vector3(3.375528f, 3.375528f, 3.375528f);
            }
        }
        if (Application.loadedLevelName == "InGame")
        {

            try
            {
                //Debug.Log("_semester:" + _semester);
         
               // Debug.Log("Line 119 Semester = "+ Semester);
                //Debug.Log("Line 119 _semester = " + _semester);
               

                    // 세 리스트를 모두 update
                    UpdateClassList();
                    // event update
                    UpdateEventList();
                    UpdatePrepList();
                    // group update
                    UpdateGroupList();

                   // Debug.Log("Line 139");
               
                
            }
            catch (NullReferenceException e)
            {
                Debug.Log("Hey Null");
                Debug.Log(e.Message);
                if (GameManager == null) {
                    Debug.Log("GameManager is Null");
                }
            }
        }

        //UI menu 생성
        try
        {
            int workingslime = GameObject.Find("GameManager").GetComponent<ClassManager>().workingSlime;
            workingslime += GameObject.Find("GameManager").GetComponent<SocialManager>().workingSlime;
            GameObject.Find("GameManager").GetComponent<UIManager>().knowledgeStet = GPA;//InteligenceStat;
            GameObject.Find("GameManager").GetComponent<UIManager>().socialStet = SocialStat;
            GameObject.Find("GameManager").GetComponent<UIManager>().workingSlime = workingslime;
            GameObject.Find("GameManager").GetComponent<UIManager>().restSlime = SlimeNum - workingslime;
            for (int i = 0; i < groupList.Count; i++) {
                if ((string)groupList[i] == "CSE" || (string)groupList[i] == "EEE")
                {
                    GameObject.Find("GameManager").GetComponent<UIManager>().dept = (SocialManager.groupname)GameObject.Find("GameManager").GetComponent<SocialManager>().getgroupcode((string)groupList[i]);
                    break;
                }
            }
            GameObject.Find("GameManager").GetComponent<UIManager>().classNum = ClassList.Count;
            GameObject.Find("GameManager").GetComponent<UIManager>().socialNum = groupList.Count;
        }
        catch (NullReferenceException e)
        {
            //if (GameManager == null) Debug.Log("The GameManager is null");
            //else Debug.Log("null exception");
        }

    }

    private void UpdateCurCredit()
    {
        CurCredit = 0;
        for(int i = 0; i < ClassList.Count; i++)
        {
            CurCredit += ((ClassNode)ClassList[i]).level + 1;
        }
    }

    private void UpdateEventList()
    {

        ArrayList newEvents = GameManager.GetComponent<SocialManager>().EventList;
        if (newEvents == null)
        {
            eventList = newEvents;
            eventNum = 0;
            return;
        }

        if (newEvents.Count < eventList.Count)        // 이벤트가 종료됨
        {
            oldEventList.Add(eventList[eventList.Count - 1]);
        }

        eventList = newEvents;
        eventNum = eventList.Count;
    }
    private void UpdatePrepList()
    {

        ArrayList newPreps = GameManager.GetComponent<SocialManager>().PrepList;
        if (newPreps == null)
        {
            prepList = newPreps;
            prepNum = 0;
            return;
        }
  
        prepList = newPreps;
        prepNum = prepList.Count;
    }
    private void UpdateGroupList()
    {
        ArrayList newGroups = GameManager.GetComponent<SocialManager>().InGroup;
        if (newGroups == null)
            return;
        if (newGroups.Count != groupNum)     // 새로운 그룹에 들어가거나 나왔을때만 groupList update
            groupList = newGroups;
        groupNum = groupList.Count;
    }
    private void UpdateClassList()
    {
        ClassList = GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_list;
        
        classNum = ClassList.Count;   

    }

 

    // 밑의 계산 식은 업데이트 필요
    public void UpdateInteligence(int credit,float point)
    {
        InteligenceStat += credit * point;
        CurCredit -= credit;
        OldCredit += credit;
        if (OldCredit != 0) GPA = InteligenceStat / OldCredit;
    }
    public void UpdateSocial(float point)
    {
        SocialStat += point;
    }


    public void Save()
    {
        string path = pathForDocumentsFile("savedata.dat");

        dataTable = new Hashtable();
        Debug.Log(path);
        dataTable.Add("classList", GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_list);
        Debug.Log(1);
        dataTable.Add("oldClassList", GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_oldList);
        dataTable.Add("department", GameObject.Find("SignedClassList").GetComponent<SignedClassList>().department);
        Debug.Log(1);
        dataTable.Add("groupList", groupList);
        Debug.Log(1);
        //dataTable.Add("eventList", eventList);
        //Debug.Log(eventList);
        //dataTable.Add("prepList", prepList);
        Debug.Log(1);
        dataTable.Add("iStat", InteligenceStat);
        Debug.Log(1);
        dataTable.Add("sStat", SocialStat);
        Debug.Log(1);
        dataTable.Add("slimeNum", slimeNum);
        Debug.Log(1);
        dataTable.Add("semester", Semester);
        Debug.Log(1);
        dataTable.Add("time", Time);

        var b = new BinaryFormatter();
        var f = File.Create(path);
        
        tempList = dataTable;
        b.Serialize(f, tempList);
    }

    public void Load()
    {
        ClassList = new ArrayList();
        oldEventList = new ArrayList();
        eventList = new ArrayList();
        prepList = new ArrayList();

        string path = pathForDocumentsFile("savedata.dat");
        if (File.Exists(path))
        {
            var b = new BinaryFormatter();
            var f = File.Open(path, FileMode.Open);
            obj = b.Deserialize(f);
            f.Close();              // data.dat에 저장된 binary형식을 불러와서 obj에 저장함

            dataTable = obj as Hashtable;

            // obj에 저장된 값들 다시 불러오기~

            ClassList = dataTable["classList"] as ArrayList;
            oldClassList = dataTable["oldClassList"] as ArrayList;
            groupList = dataTable["groupList"] as ArrayList;
            GameObject.Find("SignedClassList").GetComponent<SignedClassList>().department = (int)dataTable["department"];
           // eventList = dataTable["eventList"] as ArrayList;
            //prepList = dataTable["prepList"] as ArrayList;
            classNum = ClassList.Count;
            groupNum = groupList.Count;
            eventNum = eventList.Count;
            prepNum = prepList.Count;
            InteligenceStat = (float)dataTable["iStat"];
            SocialStat = (float)dataTable["sStat"];
            SlimeNum = (int)dataTable["slimeNum"];
            Semester = (int)dataTable["semester"];
            Semester = Semester - 1;

            // InGame scene으로 넘어온 다음, 다른 manager들에게 값을 넘겨주는 loaddata함수 호출

            Application.LoadLevel("InGame");
            LoadData();

        }
    }
    private string pathForDocumentsFile(string filename)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            string path = Application.persistentDataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path,filename);
        }
        else
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path,filename);
        }

    }
    private void LoadData()             // '//' 표시해둔 부분은 그 manager안에서 set속성을 줘야 함
    {
        GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_list = ClassList;
        GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_oldList = oldClassList;
        // GameManager.GetComponent<SocialManager>().EventList = eventList;
        //GameManager.GetComponent<SocialManager>().PrepList = prepList;
       

        
        
    }

    public Boolean Ending()       // 매 학ㄱ가 종료되면 부름
    {
        Boolean isEnd = false;
        Boolean isearly = false;
        int num =  GameObject.Find("SignedClassList").GetComponent<SignedClassList>().data_oldList.Count;
        int maxNum = GameObject.Find("SignedClassList").GetComponent<SignedClassList>().allList.Count;
        if (Semester >= 10 && num<maxNum) // || GameManager.GetComponent<ClassNode>().isdone == false
        {
            isEnd = true;
            endingName = "졸업실패 ㅠㅗㅠ";
            endingContent = "10학기동안 과목을 모두 수강하지 못했다... 이제는 졸업이 아니라 수료 ㅠㅠ";
            PlayerPrefs.SetString("ending1", endingName);
            PlayGamesPlatform.Instance.ReportProgress("CgkIy-LC4v0OEAIQAg", 100.0f, (bool success) => {
            });
        }
        else if (Semester < 10 && num >= maxNum)             // 각 스택에 대한 수치는 고쳐야 함
        {
            if (Semester < 8)
            {
                isearly = true;
                if (Semester == 1) {
                    PlayGamesPlatform.Instance.ReportProgress("CgkIy-LC4v0OEAIQCA", 100.0f, (bool success) => {
                    });
                }
            }
                
            isEnd = true;
            // 조건문 -> 테크트리 완료
            if (GPA > 3.7f && SocialStat > 50)      // 모든 스탯 기준 이상
            {
                endingName = "성공적인 졸업";
                endingContent = "학교를 다니면서 학점도, 친구도 어느 하나 놓치지 않았다 ^오^";
                PlayerPrefs.SetString("ending2", endingName);
                PlayGamesPlatform.Instance.ReportProgress("CgkIy-LC4v0OEAIQBw", 100.0f, (bool success) => {
                });
            }
            else if (GPA > 4.0f)               // 지식만 높음
            {
                endingName = "학점왕 졸업";
                endingContent = "수업과 학점에 매달려서 4점대는 넘었지만 친구가 없다 ㅠㅠ";
                PlayerPrefs.SetString("ending3", endingName);
                PlayGamesPlatform.Instance.ReportProgress("CgkIy-LC4v0OEAIQBQ", 100.0f, (bool success) => {
                });
            }
            else if (SocialStat > 80)                    // 인맥만 높음
            {
                endingName = "인맥왕 졸업";
                endingContent = "학점은 아쉽지만 평생 갈 친구들은 남았다";
                PlayerPrefs.SetString("ending4", endingName);
                PlayGamesPlatform.Instance.ReportProgress("CgkIy-LC4v0OEAIQBg", 100.0f, (bool success) => {
                });
                if (SocialStat >= 836) {
                    PlayGamesPlatform.Instance.ReportProgress("CgkIy-LC4v0OEAIQCQ", 100.0f, (bool success) => {
                    });
                }
            }
            else
            {
                endingName = "보통 졸업";
                endingContent = "걍 살았다";
                PlayerPrefs.SetString("ending5", endingName);
                PlayGamesPlatform.Instance.ReportProgress("CgkIy-LC4v0OEAIQBA", 100.0f, (bool success) => {
                });
            }
        }
        
        
        Debug.Log(isEnd);
        
            return isEnd;
    }

    public void goTutorial()
    {
        DestroyObject(GameObject.Find("PlayerManager"));
        Application.LoadLevel("Tutorial");

    }

    public void showEndingList()
    {
        DestroyObject(GameObject.Find("PlayerManager"));
        Application.LoadLevel("EndingList");
    }

    public void showCredit()
    {
        DestroyObject(GameObject.Find("PlayerManager"));
        Application.LoadLevel("Credit");
    }

    IEnumerator getImage() {
        yield return StartCoroutine(((PlayGamesLocalUser)Social.localUser).LoadImage());
        PlayerProfile = ((PlayGamesLocalUser)Social.localUser).image;
        PlayerProfile = ResizeTexture(PlayerProfile, 1040, 840);
    }

    private Texture2D ResizeTexture(Texture2D source, int targetWidth, int targetHeight)
    {
        Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, true);
        Color[] rpixels = result.GetPixels(0);
        float incX = (1.0f / (float)targetWidth);
        float incY = (1.0f / (float)targetHeight);
        for (int px = 0; px < rpixels.Length; px++)
        {
            rpixels[px] = source.GetPixelBilinear(incX * ((float)px % targetWidth), incY * ((float)Mathf.Floor(px / targetWidth)));
        }
        result.SetPixels(rpixels, 0);
        result.Apply();
        return result;
    }
}
