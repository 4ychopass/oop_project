===============================================================================================
문서 최종 작성일: 2015/10/22
본 프로젝트는 2015년 POSTECH 2학기 CSED232 과목의 4조(Team 4YCHO-PASS)의 프로젝트임을 밝힙니다.
===============================================================================================

=============
프로젝트 정보
=============

-VCS: Git(https://bitbucket.org/4ychopass/oop_project.git)
-Game Engine: Unity 2D(ver 5.2.1f1)
-Source Edditor: Visual Studio 2015 Community (Visual Studio 2015 Tools for Unity)
-Language: C#
-OS: Android(API21)
-Graphic: Photoshop CS6

================
각 디렉토리 설명
================
/.gitignore
	Unity와 Git을 연동하는데 있어서 문제가 될 수 있는 파일들을 미리 적어 전송을 막아두었다.

/README.txt
	본 문서에 해당하는 파일로 본 프로젝트에 대한 간략한 설명들을 적어두었다.

/OOP_project
	Unity project의 실질적인 루트 디렉토리이다.

/OOP_project/Assets
	Unity project에 필요한 각종 파일들을 모아둔 곳이다.

/OOP_project/Assets/Material
	개발에 필요한 Material들을 모아둔 곳이다.

/OOP_project/Assets/Package
	각종 Package파일들을 모아둔 곳이다. 

/OOP_project/Assets/Prefab
	각종 Prefab들을 모아둔 디렉토리이다.

/OOP_project/Assets/Resource
	개발에 필요한 리소스(이미지, 폰트, 소리)를 모아둔 디렉토리이다.

/OOP_project/Assets/Resource/Font
	개발에 필요한 폰트를 모아둔 디렉토리이다.

/OOP_project/Assets/Resource/Image
	개발에 필요한 이미지 파일들을 모아둔 디렉토리이다.

/OOP_project/Assets/Resource/Sound
	개발에 필요한 음성 파일들을 모아둔 디렉토리이다.

/OOP_project/Assets/Scene
	각종 Scene들을 모아둔 디렉토리이다.

/OOP_project/Assets/Script
	각종 Script들을 모아둔 디렉토리이다.
